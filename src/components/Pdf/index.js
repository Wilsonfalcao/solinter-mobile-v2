import React, { useEffect } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import * as Sharing from 'expo-sharing';
import * as FileSystem from 'expo-file-system';
import { St} from 'expo-file-system';
import PDFReader from 'rn-pdf-reader-js';

const Pdf = ({navigation, route}) => {
    const { id } = route.params;
    useEffect(() => {
        navigation.setOptions({
            headerRight:() => (
                <TouchableOpacity style={{padding:10}} activeOpacity={1} onPress={() => printer()}>
                    <MaterialIcons name="drive-folder-upload" color="black" size={24} />
                </TouchableOpacity>
            )
        })
    },[navigation]);

    return (
        <SafeAreaView style={{flex:1}}>
            <PDFReader source={{uri: 'https://www.solinter.com.br/orcamento/orcamento/visualiza_orcamento_pdf/'+id}}/>
        </SafeAreaView>
    )

    async function printer(){
        Sharing.isAvailableAsync().then(available => {
            if(available){
                const fileUri = FileSystem.cacheDirectory + "" + id + ".pdf";
                const options = {
                    mimeType: "application/pdf",
                    dialogTitle: 'Compartinhar',
                    UTI: "application/pdf",
                }; 
                var midia = 'https://www.solinter.com.br/orcamento/orcamento/visualiza_orcamento_pdf/'+id;
                FileSystem.downloadAsync(midia, fileUri).then( async () => await Sharing.shareAsync(fileUri, options))        
                
            }
        })
        if(Sharing.isAvailableAsync()){
            var url = "https://www.solinter.com.br/orcamento/orcamento/visualiza_orcamento_pdf/"+id;
            Sharing.shareAsync(url, null).then(() => {})
        }
    }
}

export default Pdf;