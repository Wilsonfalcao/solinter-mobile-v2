import React from 'react';
import { ActivityIndicator, Modal, StyleSheet, Text, View } from 'react-native';
import { Styles } from '../../config/styles';

const Loading = (props) => {
    const  { visible, message }= props
    return (
        <Modal
            animationType={'fade'}
            transparent={true}
            visible={visible}>
            <View style={styles.container}>
                <View style={styles.wrapper}>
                    <ActivityIndicator color={'black'} />
                </View>
            </View>
        </Modal>
    );
}

const  styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'rgba(0,0,0,0.2)'
    },
    wrapper:{
        backgroundColor:'white',
        borderRadius:10,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        padding:16,
        elevation:2,
        shadowRadius:0.2,
        shadowOpacity:0.1,
        shadowOffset:{width:1, height:2},
        shadowColor:'grey'
    }
})

export default Loading;