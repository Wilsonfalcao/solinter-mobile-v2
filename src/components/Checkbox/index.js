import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Styles } from '../../config/styles';

const Checkbox = (props) => {
    const { select, setSelect, title } = props;

    return (
        <TouchableOpacity 
        activeOpacity={1}
        style={{flexDirection:'row', alignItems:'center'}} 
        onPress={() => setSelect(!select)} >
            <View style={{...styles.item, backgroundColor:select ? Styles.colors.primary : 'transparent' }} />
            <Text style={styles.text} numberOfLines={1}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    item:{
        width: 18,
        height: 18,
        borderRadius: 5,
        marginRight:5,
        borderColor: Styles.colors.primary,
        borderWidth:2
    },
    text:{
        fontSize:12,
        color:'black'
    }
})

export default Checkbox;