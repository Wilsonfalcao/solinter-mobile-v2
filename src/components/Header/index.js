import React, { useContext } from 'react';
import { View, Image, Text, TouchableOpacity, StyleSheet, Dimensions, SafeAreaView, StatusBar } from 'react-native';
import { MaterialIcons } from "@expo/vector-icons";
import { useNavigation } from '@react-navigation/native';
import { useIsDrawerOpen } from '@react-navigation/drawer';
import { Styles } from '../../config/styles';
import ProfileContext from '../../context/ProfileContext';

const { width } = Dimensions.get('screen');

const Header = () => {
    
    const [profile] = useContext(ProfileContext);

    const navigation = useNavigation();
    const isDrawerOpen = useIsDrawerOpen();

    return (
        <View style={styles.header}>
            <SafeAreaView style={{backgroundColor:'white'}} />
            <StatusBar backgroundColor={isDrawerOpen ? '#363f4d' : 'white' } barStyle={ isDrawerOpen ? 'light-content' : 'dark-content' } />
            <TouchableOpacity onPress={()=>{navigation.toggleDrawer()}}>
                <MaterialIcons name={'menu'} size={30} color={Styles.colors.secondary} />
            </TouchableOpacity>
            <View style={styles.wrapper}>
                <TouchableOpacity style={styles.box} onPress={()=> openPerfil()}>
                    <Text style={styles.name}>{profile.name}</Text>
                    <Text numberOfLines={1} style={styles.business}>{profile.business}</Text>
                </TouchableOpacity>
                <Image
                style={styles.logo}
                source={require('../../../assets/logo.png')}
                resizeMode={'contain'} />
            </View>
        </View>
    );

    async function openPerfil(){
        navigation.navigate('Perfil');
    }
}

const styles = StyleSheet.create({
    header:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        backgroundColor:'white',
        paddingVertical:10,
        paddingHorizontal:16,
        elevation:2,
        shadowColor:'grey',
        shadowOffset:{width:0, height:2},
        shadowOpacity:0.1,
        shadowRadius:0.3,
        zIndex:1000
    },
    logo:{
        width:(width / 7),
        height:(width / 8)
    },
    wrapper:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start'
    },
    box:{
        flex:1,
        flexDirection:'column',
        marginHorizontal:16
    },
    name:{
        fontSize:15,
        color:"#303030",
        fontWeight:'700',
        textTransform:'uppercase'
    },
    business:{
        fontSize:13,
        color:"#707070",
        fontWeight:'400',
        textTransform:'uppercase',
    },
})

export default Header;