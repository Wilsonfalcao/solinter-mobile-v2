import { StatusBar } from 'expo-status-bar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useContext } from 'react';
import { SafeAreaView, StyleSheet, View, Text, StatusBar as Bar, Platform, Image, Dimensions, ScrollView, TouchableOpacity, Alert} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Styles } from '../../config/styles'

import Header from '../Header';
import ProfileContext from '../../context/ProfileContext';

const Menu = ({navigation}) => {

    const profile = useContext(ProfileContext);

    function exitToApp() {
        new Alert.alert("Aviso", "Deseja realmente sair?", [
            {text:'Não'},
            {text:'Sim', onPress: async ()=>{
            await AsyncStorage.removeItem("profile");
                navigation.reset({
                    index:0,
                    routes:[{name:'Login'}]
                })
            }}
        ])
    }

    return (
        <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor={'#4682B4'} style={'light'} />
            <Header profile={profile} />
            <ScrollView style={styles.wrapper} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} overScrollMode={'never'}>
                <View style={{flex:1, flexDirection:'column'}}>
                    <View style={{flex:1}}>
                        {
                            profile.modulos.length > 0 ?
                            <View>
                                { profile.modulos.map( item => {
                                    if(item.codigomodulo == 21){
                                        return(
                                            <TouchableOpacity key={item.codigomodulo} style={styles.item} onPress={()=>{
                                                    navigation.navigate(`${item.codigomodulo}`, { profile, rotinas : item.rotinas })
                                                }}>
                                                <MaterialIcons name={'shopping-cart'} color={Styles.colors.grey} size={20}/>
                                                <Text style={styles.title}>{item.nomemodulo.toUpperCase()}</Text>
                                                <MaterialIcons name={'arrow-right'} color={Styles.colors.grey} size={15}/>
                                            </TouchableOpacity>
                                        )
                                    }
                                })
                                }
                            </View>
                            :
                            <View style={{flex:1, alignItems:'center', justifyContent:'center', height:Dimensions.get('screen').height - 100}}>
                                <Text style={{color:'grey', textAlign:'center'}}>Você não tem modulos {'\n'} disponíveis</Text>
                            </View>
                        }
                    </View>
                </View>
            </ScrollView>
            <View style={{backgroundColor:'#f3f3f3'}}>
                <TouchableOpacity activeOpacity={0.5} onPress={()=>exitToApp()}>
                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', paddingHorizontal:16, paddingVertical:10, margin:10, backgroundColor:'#f3f3f3', borderRadius:5}}>
                        <MaterialIcons name={'exit-to-app'} color={'red'} size={25} />
                        <Text style={{fontSize:16, color:'red', marginHorizontal:16}}>Sair</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'#4682B4',
        marginTop:Platform.OS === "android" ? Bar.currentHeight : 0
    },
    wrapper:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'#f3f3f3'
    },  
    header:{
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'#4682B4',
        paddingHorizontal:10,
        paddingVertical:5
    },
    logo:{
        width:Dimensions.get('screen').width / 6,
        height:Dimensions.get('screen').width / 6,
        marginBottom:5
    },
    name:{
        fontSize:14,
        color:'white',
        fontWeight:'bold',
        textTransform:'uppercase'
    },
    business:{
        fontSize:12,
        color:'rgba(255,255,255,0.8)',
        textTransform:'uppercase'
    },
    item:{
        flexDirection:'row',
        alignItems:'center',
        padding:20,
        borderBottomColor:'#c3c3c3',
        borderBottomWidth:1
    },
    title:{
        flex:1,
        fontSize:16,
        color:Styles.colors.secondary,        
        marginHorizontal:16
    }
})

export default Menu;


