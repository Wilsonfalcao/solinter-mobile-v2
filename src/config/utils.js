export const Utils = {
    formatMoeda : (valor) => {
        if(valor){
            valor = valor + '';
            valor = parseInt(valor.replace(/[\D]+/g,''));
            valor = valor + '';
            valor = valor.replace(/([0-9]{2})$/g, ",$1");
            if (valor.length > 6) {
                valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
            }
            return valor;
        }
        return valor;
    },
    formatCurrency:(valor)=>{
        return parseFloat(valor).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    },
    convertMoedaToFloat: (value) => {
        if(value){
            value = parseFloat(value.replace('.', '').replace(',', '.'))
            return value;
        }
        return 0;
    },
    parseToFloat:(value)=>{
        let valorEntradaFloat = value.replace(".","");
            valorEntradaFloat = valorEntradaFloat.replace(",",".");
            valorEntradaFloat = parseFloat(valorEntradaFloat);
            return valorEntradaFloat;
    },
    timePretty: (data) => {
        const date = new Date();
        let diaDate = date.getDate();
        let mesDate = (date.getMonth() + 1);
        let anoDate = date.getFullYear();

        diaDate = diaDate < 10 ? "0"+diaDate : diaDate;
        mesDate = mesDate < 10 ? "0"+mesDate : mesDate;

        let dateNow = [diaDate, mesDate, anoDate].join('/');

        const dateTime = new Date(data);
        let diaDateTime = dateTime.getDate();
        let mesDateTime = (dateTime.getMonth() + 1);
        let anoDateTime = dateTime.getFullYear();

        diaDateTime = diaDateTime < 10 ? "0"+diaDateTime : diaDateTime;
        mesDateTime = mesDateTime < 10 ? "0"+mesDateTime : mesDateTime;

        let dateTimeNow = [diaDateTime, mesDateTime, anoDateTime].join('/');

        let hora = dateTime.getHours();
        let min = dateTime.getMinutes();
        hora = hora < 10 ? "0"+hora : hora;
        min = min < 10 ? "0"+min : min;

        if(dateNow === dateTimeNow){
            return "Hoje às "+[hora, min].join(":");
        }else{
            return dateTimeNow+" às "+[hora, min].join(":");
        }
    },
    mascaraTelefone: (v) => {
        if(v != null){
            v=v.replace(/\D/g,"");
            v=v.replace(/^(\d{2})(\d)/g,"($1) $2");
            v=v.replace(/(\d)(\d{4})$/,"$1-$2");
            return v;
        }
        return "-";
    }
}