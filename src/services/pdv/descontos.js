
import { API } from "../../config/api";

export const ServiceDescontos =  {
    getDescontos : async (token) => {
        const req =  await fetch(API.mobile+"/discount/getDiscount",{
            method:'get',
            headers:{                
                'Content-Type':'application/json',
                'token':`${token}`
            }
        });
        return await  req.json();
    },
    updateItemDescontos : async (token, id, status, motivo = null) => {
        const info = { id, status, motivo };
        const req =  await fetch(API.mobile+"/discount/discountAcceptDecline",{
            method:'put',
            headers:{
                'Content-Type':'application/json',
                'token':`${token}`
            },
            body:JSON.stringify(info)
        });
        return await  req.json();
    }
}