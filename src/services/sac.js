import { API } from "../config/api";

async function getLista(token){
    const req = await fetch(API.sac+"/whatsapp/chat/listagemDeConversas",{
        method:'get',
        headers:{
            'Content-Type':'application/json',
            'token':`${token}`
        }
    });
    return await req.json();
}

async function finishCall(token, id){
    const req = await fetch(API.sac+"/whatsapp/chat/finalizarAtendimento",{
        method:'post',
        headers:{
            'Content-Type':'application',
            'token':`${token}`
        },
        body:JSON.stringify({id})
    });
    return await req.json();
}

async function listaChat(token, id){
    const req = await fetch(API.sac+"/whatsapp/chat/listaChatPorAtendimento",{
        method:'post',
        headers:{
            'Content-Type':'application',
            'token':`${token}`
        },
        body:JSON.stringify({id})
    });
    return await req.json();
}

async function registroMensagem(token, atendimento, client, type, midia, message){
    const req = await fetch(API.sac+"/whatsapp/chat/registrarChatAtendimento",{
        method:'post',
        headers:{
            'Content-Type':'application',
            'token':`${token}`
        },
        body:JSON.stringify({atendimento, client, type, midia, message})
    });
    return await req.json();
}

async function listaAtendentes(token){
    const req = await fetch(API.sac+"/whatsapp/chat/listaAtendentes",{
        method:'post',
        headers:{
            'Content-Type':'application',
            'token':`${token}`
        }
    });
    return await req.json();
}

async function transfereAtendimento(token, conversa, atendente){
    const req = await fetch(API.sac+"/whatsapp/chat/transferirAtendimento",{
        method:'post',
        headers:{
            'Content-Type':'application',
            'token':`${token}`
        },
        body:JSON.stringify({id: conversa.id, novo_atendente: atendente.id})
    });
    return await req.json();
}


async function sendMessageText(usuario, texto){

    const req = await fetch(API.socket+"/message",{
        method:'post',
        headers:{'Content-Type':'application/json'},
        body:JSON.stringify({ usuario, texto })
    });
    return await req.json();

}

const getListCall = async (CallBack) =>{
    return await fetch('https://www.solinter.com.br/v2.0/api/whatsapp/chat/getListTalk', {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${profile.accessToken}`
            },
            body:JSON.stringify({ cd_conversa: dados.id })
        }).then(data=>data.json())
        .then(JSON=>CallBack(JSON));
}


export{
    getLista,
    finishCall,
    listaChat,
    registroMensagem,
    listaAtendentes,
    transfereAtendimento,
    sendMessageText,
    getListCall
}