import { API } from "../config/api";

class Orcamentos_Api {

    constructor(token){
        this.token = token;
    }
    
    async getListAll() {
        const req = await fetch(API.mobile+"/orcaments/listAllOrcaments", {
            method:'get',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            }
        })
        return await req.json();
    }   

    async editOrcamento(_bodyJSON){

        const req = await fetch(API.mobile+"/orcaments/editOrcament", {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            },
            body:JSON.stringify(_bodyJSON)
        })
        return await req.json();
    }
    
    async createOrcament(name, email, phone, whatsapp, city, state, condition, parcells, entry, priceEntry, itens){

        var dados  = {
            nameClient: name,
            emailClient: email,
            phoneNumber: phone,
            zapNumber: whatsapp,
            cityClient: city,
            stateClient: state.uf,
            conditionPayment: condition.id,
            parcelsPayment: parcells,
            entryPayment: (entry === 0 || entry == -1) ? false : true,
            priceEntryPayment: priceEntry ? parseFloat(priceEntry) : 0,
            descriptionPayment: condition.descricao,
            itens
        }

        const req = await fetch(API.mobile+"/orcaments/createNewOrcament", {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            },
            body: JSON.stringify(dados)
        })
        return await req.json();
    }

    async getAllProductsAndServices(){
        const req = await fetch(API.mobile+"/orcaments/listAllProductsOrServicesOrcaments", {
            method:'get',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            }
        })
        return await req.json();
    }

    async removeOrcament(id) {
        const req = await fetch(API.mobile+"/orcaments/deleteOrcament", {
            method:'delete',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            },
            body:JSON.stringify({id})
        })
        return await req.json();
    }

    async detalisOrcament(id){
        const req = await fetch(API.mobile+"/orcaments/detailOrcament/"+id, {
            method:'get',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            }
        })
        return await req.json();
    }

    async sendEmail(id){
        const req = await fetch(API.mobile+"/orcaments/sendOrcamentsToClient", {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            },
            body:JSON.stringify({ id })
        })
        return await req.json();
    }

    async print(id){
        const req = await fetch("https://www.solinter.com.br/orcamento/orcamento/visualiza_orcamento_pdf"+id, {
            method:'get',
            headers:{
                'Content-Type':'octstream/pdf'
            }
        })
        return req;
    }

    async changeStatus(id, status){
        const req = await fetch(API.mobile+"/orcaments/alterStatusOrcaments", {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            },
            body:JSON.stringify({ id, status })
        })
        return await req.json();
    }

    async buscarProduto(search, condicao_pagamento_id, numero_parcelas){
        const req = await fetch(API.mobile+"/orcaments/listProdutsAndKits", {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            },
            body:JSON.stringify({ search, idcond_pagto: condicao_pagamento_id, qtd_vezes: numero_parcelas })
        })
        return await req.json();
    }

    async buscarServicos(search, condicao_pagamento_id, numero_parcelas){
        const req = await fetch(API.mobile+"/orcaments/listServices", {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            },
            body:JSON.stringify({ search, idcond_pagto: condicao_pagamento_id, qtd_vezes: numero_parcelas })
        })
        return await req.json();
    }

    async salvarServico(descricao){
        const req = await fetch(API.mobile+"/orcaments/saveOrcamentsServices", {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            },
            body:JSON.stringify({ descricao })
        })
        return await req.json();
    }
}

export default Orcamentos_Api