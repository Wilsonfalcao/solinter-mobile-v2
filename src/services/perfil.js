import { API } from "../config/api";
async function changeInfoPerfil(token, unidade, usuario){
    console.log(usuario)
    const req = await fetch(API.mobile+"/unidades/alterSession", {
        method:'post',
        headers:{
            'Content-Type':'application/json',
            'token':`${token}`
        },
        body:JSON.stringify({unit: unidade, user: usuario.id})
    });
    return await req.json()
}

export {
    changeInfoPerfil
}