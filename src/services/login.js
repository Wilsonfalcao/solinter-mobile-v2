import { API } from "../config/api"

async function validateLogin(email, password, token_fcm = null){
    const req = await fetch(API.system+"/login", {
        method:'post',
        headers:{
            'Content-Type':'application/json'
        },
        body:JSON.stringify({email, password, token_fcm: PushNotificationToken})
    });
    console.log(JSON.stringify({email, password, token_fcm: PushNotificationToken}));
    return await req.json();
}

export {
    validateLogin
}