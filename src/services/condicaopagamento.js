import { API } from "../config/api";

class CondicaoPagamentoService {

    constructor(token){
        this.token = token;
    }

    async getList(){
        const req = await fetch(API.mobile+"/condpagto/getCondPagtoOrcamento", {
            method:'get',
            headers:{
                'Content-Type':'application/json',
                'token':`${this.token}`
            }
        })
        return await req.json();
    }
}
export default CondicaoPagamentoService;