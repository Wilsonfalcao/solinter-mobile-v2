import React from 'react';
import { View, Modal, TouchableOpacity, StyleSheet, Text, Platform } from 'react-native';
import { useHeaderHeight } from '@react-navigation/stack'

const PopupMenu = (props) => {
    const { visible } = props;
    const close = props.close.bind();

    const headerHeight = useHeaderHeight();

    var itens = [
        {
            id:1,
            name:'Transferir atendimento',
            action:'transfer'
        },
        {
            id:2,
            name:'Finalizar atendimento',
            action:'finish'
        }
    ]

    return (
        <Modal 
        visible={visible}
        transparent={true}
        animationType={'none'}>
            <View style={{flex:1}}>
                <TouchableOpacity style={{flex:1}} onPress={() => close(null)} activeOpacity={1} />
                <View style={styles.box}>
                    {
                        itens.map(item => (
                            <TouchableOpacity key={item.id} style={styles.button} onPress={() => close(item)}>
                                <Text style={{fontSize:15, color:'black', fontWeight:'500'}}>{item.name}</Text>
                            </TouchableOpacity>
                        ))
                    }
                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    box:{
        backgroundColor:'white',
        paddingVertical:10,
        paddingHorizontal:16,
        position:'absolute',
        top: Platform.OS === "ios" ? 72 : 65, right:10,
        elevation:2,
        shadowOpacity:0.3,
        shadowOffset:{
            width:0,
            height:2
        },
        shadowRadius:0.1,
        shadowColor:'grey',
        borderRadius:5
    },
    button:{
        paddingVertical:10
    }
})

export default PopupMenu;