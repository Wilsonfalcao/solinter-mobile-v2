import React, { useContext, useEffect } from 'react';
import { Dimensions, Image, StyleSheet, View } from 'react-native';
import ProfileContext from '../../context/ProfileContext';

const { width } = Dimensions.get('screen');

const Splash = ({navigation}) => {

    const [profile] = useContext(ProfileContext);

    useEffect(()=> openApp(), []);

    function openApp(){
        navigation.replace( profile.accessToken ? 'Principal' : 'Login' );
    }

    return (
        <View style={styles.container}>
            <Image style={styles.logo} resizeMode={'contain'} source={require('../../../assets/logo.png')} />
        </View>
    );
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center'
    },
    logo:{
        width: (width / 2),
        height: (width / 2)
    }
})

export default Splash;