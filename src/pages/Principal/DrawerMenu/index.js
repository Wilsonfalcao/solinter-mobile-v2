import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { memo, useContext, useEffect } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, View, Text, Platform, StatusBar, Image, Alert, SafeAreaView} from 'react-native';
import { MaterialIcons } from "@expo/vector-icons";
import { useNavigation } from '@react-navigation/native';
import { versao } from '../../../config/strings';

import ProfileContext from "../../../context/ProfileContext";
import MenuDrawerContext from '../../../context/MenuDrawerContext';

const DrawerMenu = () => {

    const [ profile ] = useContext(ProfileContext);
    
    const [menu, setMenu] = useContext(MenuDrawerContext);

    const navigation = useNavigation();

    useEffect(() => {
        ListaModulos();
    },[navigation])

    return (
        <SafeAreaView style={{...styles.container, backgroundColor:'#363f4d'}}>
            <View style={{flexDirection:'column', paddingTop:Platform.OS === "android" ? StatusBar.currentHeight : 5, paddingBottom:16, paddingHorizontal:16}}>
                <Image
                style={{width:80, height:80}}
                resizeMode={'contain'}
                resizeMethod={'resize'}
                source={require('../../../../assets/logo.png')} />
                <View style={{flexDirection:'column'}}>
                    <Text style={{color:'white', fontSize:14, fontWeight:'bold'}}>{profile.name}</Text>
                    <Text numberOfLines={1} style={{color:'white', fontSize:11}}>{profile.business}</Text>
                </View>
            </View>
            <ScrollView style={{flex:1}}>
                <View style={{flex:1, flexDirection:'column'}}>
                    
                    <View style={{flexDirection:'column'}}>
                        <TouchableOpacity onPress={() => { navigation.navigate('Dashboard')}} activeOpacity={1} style={{...styles.modulo}}>
                            <MaterialIcons name={'dashboard'} color={'white'} size={20} />
                            <Text style={{...styles.title_modulo, flex:1}}>Dashboard</Text>
                        </TouchableOpacity>
                    </View>

                    {
                        menu.map( item => (
                            <View key={item.codigomodulo} style={{flexDirection:'column'}}>
                                <TouchableOpacity onPress={() => selectItemMenu(item)} activeOpacity={1} style={{...styles.modulo, backgroundColor:item.select ? 'rgba(255,255,255,0.1)' : 'transparent'}}>
                                    <MaterialIcons name={'dashboard'} color={'white'} size={20} />
                                    <Text style={{...styles.title_modulo, flex:1}}>{item.nomemodulo}</Text>
                                    {item.rotinas.length > 0 && <MaterialIcons name={'arrow-right'} color={'white'} size={18} />}
                                </TouchableOpacity>
                                { (item.rotinas.length > 0 && item.select) && 
                                    item.rotinas.map( rotina => (
                                        <TouchableOpacity key={rotina.codigorotina} style={{padding:16}} onPress={() => selectItemRotina(rotina.codigorotina)}>
                                            <Text numberOfLines={1} style={{color:'white', fontSize:15}}>{rotina.nomerotina}</Text>
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        ))
                    }
                </View>
            </ScrollView>
            <View style={{flexDirection:'column'}}>
                <TouchableOpacity style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', padding:16}} onPress={()=>exitApp()}>
                    <MaterialIcons name={'exit-to-app'} color={'white'} size={20} />
                    <Text style={{fontSize:16, color:'white', flex:1, marginHorizontal:16}}>Sair</Text>
                </TouchableOpacity>
                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-end', margin:10}}>
                    <Text style={{color:'white', fontSize:12}}>Versão: </Text>
                    <Text style={{fontSize:12, color:'white'}}>{versao}</Text>
                </View>
            </View>
        </SafeAreaView>
    );

    function ListaModulos(){
        const lista = [];
        if(profile.modulos.length > 0){
            profile.modulos.map( item => {
                lista.push({
                    codigomodulo: item.codigomodulo,
                    nomemodulo: item.nomemodulo,
                    rotinas: item.rotinas,
                    icon:'shopping-cart',
                    select: false
                })
            })
        };
        setMenu(lista);
    }

    function selectItemMenu(item){
        const novalista = menu.map( modulo => {
            modulo.select = false;
            if(modulo.codigomodulo === item.codigomodulo){
                modulo.select = !modulo.select 
            }
            return modulo;
        })
        setMenu(novalista);
    }

    function selectItemRotina(codigorotina){
        navigation.navigate(`${codigorotina}`, {profile});
    }

    function exitApp(){
        new Alert.alert("Aviso", "Deseja sair do aplicativo?",[
            {text:'Não'},
            {text:'Sim', onPress: async ()=>{
                await AsyncStorage.removeItem("profile");
                navigation.reset({
                    index:0,
                    routes:[{name:'Login'}]
                });
            }}
        ])
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    modulo:{
        paddingVertical:16,
        paddingHorizontal:16,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start'
    },
    title_modulo:{
        color:'white',
        fontSize:16,
        fontWeight:'600',
        marginHorizontal:16
    },
    rotina:{

    }
})

export default memo(DrawerMenu);