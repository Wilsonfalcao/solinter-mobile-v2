import React, { useEffect, useState } from 'react';
import { StyleSheet, SafeAreaView, View, Text, TouchableOpacity, FlatList, Alert} from 'react-native';
import { Styles } from '../../../../config/styles';
import { Utils } from '../../../../config/utils';
import { ServiceDescontos } from '../../../../services/pdv/descontos';

import Loading from '../../../../components/Loading';


const Detalhes = ({navigation, route}) => {

  const { profile, dados } = route.params;

  const [loading, setLoading] = useState(false);
  const [produtos, setProdutos] = useState(dados.itens); 

  useEffect(() => {

    navigation.setOptions({
      headerTitle:dados.status === 1 ? 'Autorizar' : 'Detalhes'
    })

  },[]);

  function EditarStatusDesconto(status){
    if(status === 1){
      new Alert.alert("Autorizar", "Deseja realmente autorizar este desconto?",[
        {text:'Não'},
        {text:'Sim', onPress:()=>{
          enviarStatusDesconto(status, null)
        }}
      ])
    }else{
      new Alert.prompt("Motivo", "Informe abaixo o motivo",[
        {text:'Voltar'},
        {text:'Enviar', onPress:(texto) => {
          if(texto != ""){
            enviarStatusDesconto(status, texto)
          }else{
            new Alert.alert("Aviso", "Informe o motivo", [{text:'Ok'}])
          }
        }}
      ],'plain-text', null, 'default');
    }
  }

  function enviarStatusDesconto(status, motivo){
    setLoading(true)
    ServiceDescontos.updateItemDescontos(profile.accessToken, dados.id, status, motivo).then( dados => {
      setLoading(false);
      navigation.pop();
    }).catch(e => {
      setLoading(false);
      new Alert.alert("Aviso", "Erro ao tentar autorizar este desconto",[
        {text:'Ok', onPress:()=>{
          navigation.pop();
        }}
      ])
    })
  }

  return (
    <SafeAreaView style={{...styles.container, backgroundColor:'white'}}>
      <FlatList
      style={{padding:10, backgroundColor:'white', borderRadius:5}}
      data={produtos}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      overScrollMode={'never'}
      keyExtractor={(item, index)=>index.toString()}
      nestedScrollEnabled={false}
      ListHeaderComponent={()=>(
      <>
          <Text style={{marginBottom:5}}>{dados.store}</Text>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
              <Text style={styles.texto}>Cliente:</Text>
              <Text style={styles.texto}>{dados.client ? dados.client : ''}</Text>
          </View>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
              <Text style={styles.texto}>Data:</Text>
              <Text style={styles.texto}>{dados.date}</Text>
          </View>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
              <Text style={{...styles.texto, color:'black'}}>Total da venda:</Text>
              <Text style={{...styles.texto, color:'black'}}>R$ {Utils.formatMoeda(dados.totalSale)}</Text>
          </View>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
              <Text style={styles.texto}>Desconto:</Text>
              <Text style={styles.texto}>R$ {Utils.formatMoeda(dados.discountTotal)}</Text>
          </View>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
              <Text style={styles.texto}>Desconto:</Text>
              <Text style={{...styles.texto, color:'green'}}>{dados.discountPercent}%</Text>
          </View>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
              <Text style={{...styles.texto, color:'black'}}>Total Liquido:</Text>
              <Text style={{...styles.texto, color:'black'}}>R$ {Utils.formatMoeda((dados.totalSale - dados.discountTotal))}</Text>
          </View>
          <View style={{height:1, backgroundColor:'#c3c3c3', marginVertical:10}} />
              <Text style={{textAlign:'center', textTransform:'uppercase', fontSize:11, fontWeight:'bold'}}>Produtos</Text>
          <View style={{height:1, backgroundColor:'#c3c3c3', marginVertical:10}} />
      </>
      )}
      renderItem={({item: row, index})=>{
          return(
              <View style={{flexDirection:'column', marginTop:10, marginBottom: index === (dados.itens.length - 1) ? 10 : 0 }}>
                  <Text style={{fontSize:12, color:'black', fontWeight:'bold'}}>{row.name.split(' - ')[1]}</Text>
                  <Text style={{fontSize:12, color:'grey'}}>Quantidade: {row.quantity}</Text>
                  <Text style={{fontSize:12, color:'grey'}}>Preço unitário: R$ {Utils.formatMoeda((row.priceUnit * row.quantity))}</Text>
                  <Text style={{fontSize:12, color:'grey'}}>Desconto: R$ {Utils.formatMoeda(row.discount)}</Text>
                  <Text style={{fontSize:12, color:'grey'}}>Custo: R$ {Utils.formatMoeda(row.cost)}</Text>
                  <Text style={{fontSize:12, color:'grey'}}>M.B(R$): R${Utils.formatMoeda(row.profit)}</Text>
                  <Text style={{fontSize:12, color:'grey'}}>M.B(%): {row.profitPercent}%</Text>
                  <Text style={{fontSize:12, color:'grey'}}>Imposto: {row.taxes > 0 ? row.taxes : '-'}</Text>
                  <Text style={{fontSize:12, color:'black', fontWeight:'bold'}}>Total: R$ {Utils.formatMoeda(row.total - row.discount)}</Text>
              </View>
          )
      }}
      ListFooterComponent={(
        <>
        {
          dados.status === 1 &&
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', marginVertical:10}}>
              <TouchableOpacity style={{...styles.button, backgroundColor:'grey'}} onPress={()=>EditarStatusDesconto(0)}>
                  <Text style={{color:'white'}}>Recusar</Text>
              </TouchableOpacity>
              <View style={{marginHorizontal:5}} />
              <TouchableOpacity style={{...styles.button, backgroundColor:Styles.colors.primary}} onPress={()=>EditarStatusDesconto(1)}>
                  <Text style={{color:'white'}}>Autorizar</Text>
              </TouchableOpacity>
          </View>
        }
        </>
      )} />

      <Loading visible={loading} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container:{
      flex:1
  },
  texto:{
      fontSize:14,
      color:'grey'
  },
  button:{
      flex:1,
      padding:12,
      alignItems:'center',
      justifyContent:'center',
      borderRadius:10,
  },
  textarea:{
      borderColor:'#c3c3c3',
      borderWidth:1,
      borderRadius:5,
      padding:10,        
      height:100,
      marginTop:10,
      textAlignVertical:'top',
      fontSize:16,
      color:'black'
  },
  button:{
      flex:1,
      backgroundColor:'grey',
      borderRadius:10,
      marginTop:10,
      alignItems:'center',
      justifyContent:'center',
      padding:13
  }
})

export default Detalhes;