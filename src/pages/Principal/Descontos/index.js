import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Lista from './Lista';
import Detalhes from './Detalhes';


const Stack = createStackNavigator();

function Descontos() {

    return (
        <Stack.Navigator initialRouteName={'Lista'}>
            <Stack.Screen name="Lista" options={{headerShown:false}} component={Lista} />
            <Stack.Screen name="Detalhes" options={{title:'Detalhes', headerTintColor:'black', headerBackTitleVisible:false}} component={Detalhes} />
        </Stack.Navigator>
    );
}

export default Descontos;