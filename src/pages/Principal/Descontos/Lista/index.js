import React, { useContext, useEffect, useState } from 'react';
import { View, SafeAreaView, StyleSheet, TouchableOpacity, Text, FlatList, ActivityIndicator, TextInput, Dimensions, StatusBar, Platform} from 'react-native';
import { Menus } from '../../../../config/menus';
import { Styles } from '../../../../config/styles';
import { ServiceDescontos } from "../../../../services/pdv/descontos";
import { Utils } from '../../../../config/utils';

import ProfileContext from '../../../../context/ProfileContext';
import Header from '../../../../components/Header';



const Lista = ({navigation}) => {

    const [profile] = useContext(ProfileContext);

    const [loading, setLoading] = useState(true);

    const [menu, setMenu] = useState(Menus.listaDescontos);
    const [indexMenu, setIndexMenu] = useState(1);

    const [descontos, setDescontos] = useState([]);
    const [lista, setLista] = useState([]);

    useEffect(() => ListaDescontos() ,[]);

    useEffect(() => {

        return navigation.addListener('focus', function(){
            itemMenuSelected(1);
            ListaDescontos();
        })

    },[navigation]);

    useEffect(() => {

        if(indexMenu === 1){
            itemMenuSelected(indexMenu)
        }

    }, [descontos]);

    function itemMenuSelected(id){
        const novo_menu = menu.map( item => {
            item.select = false;
            if(item.id === id){
                item.select = true
            }
            return item;
        });
        setMenu(novo_menu);
        setIndexMenu(id);
        filtrarDescontosIndex(id);
    }

    function filtrarDescontosIndex(index){
        if(descontos.length > 0){
            const novaLista  = descontos.filter( item => {
                if(item.status === index){
                    return item;
                }
            })
            setLista(novaLista)
        }else{
            setLista([]);
        }
    }

    function buscarDesconto(texto){
        const filtros = [];
        descontos.filter( item => {
        if(item.status != 1){
            if( item.store.toLowerCase().includes(texto.toLowerCase()) ||  
            String(item.idSale).includes(texto.toLowerCase()) || 
            (item.client != null ? item.client.toLowerCase().includes(texto.toLowerCase()) : null) ||
            (item.usuario != null ? item.usuario.toLowerCase().includes(texto.toLowerCase()) : null) ){
                filtros.push(item)
            }
        }
        }) 
        setLista(filtros)
    }

    function ListaDescontos(){
        setLoading(true);
        ServiceDescontos.getDescontos(profile.accessToken).then( dados => {
            setDescontos(dados);
            filtrarDescontosIndex(indexMenu);
            setLoading(false);            
        }).catch(e => {
            setDescontos([]);
            setLoading(false);
        })
    }

    function descontoItemSelected(row){
        navigation.navigate('Detalhes' ,{
            profile: profile,
            dados: row
        })
    }

    return (
        <SafeAreaView style={styles.container}>
            
            <Header />

            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', marginHorizontal:16, marginTop:10, marginBottom:5}}>
                <Text style={{flex:1, fontSize:16, color:'#303030', fontWeight:'bold'}}>PDV - DESCONTOS</Text>
            </View>

            <View style={{...styles.container, backgroundColor:'#f3f3f3', padding:10, marginTop:0}}>
                <View style={styles.menu}>
                    {menu.map( item => (
                        <TouchableOpacity key={item.id} style={styles.menu_item} activeOpacity={1} onPress={()=>itemMenuSelected(item.id)}>
                            <Text style={{
                                ...styles.menu_texto_item, 
                                color:item.select ? Styles.colors.primary : 'grey',
                                fontWeight: item.select ? 'bold' : 'normal'
                                }}>{item.name}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
                {
                    loading ?

                    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                        <ActivityIndicator size={'large'} color={Styles.colors.primary} />
                    </View>

                    :

                    <FlatList
                    data={lista}
                    refreshing={false}
                    onRefresh={()=>ListaDescontos(indexMenu)}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    overScrollMode={'never'}
                    nestedScrollEnabled={false}
                    keyExtractor={(item, index) => index.toString()}
                    removeClippedSubviews={true}
                    stickyHeaderIndices={[0]}
                    ListHeaderComponent={(
                        <View style={{backgroundColor:'#f3f3f3'}}>
                            {(indexMenu !== 1) &&
                            <TextInput
                            style={styles.input_buscar}
                            onChangeText={(value) => buscarDesconto(value)}
                            placeholder={'Pesquisar...'} />}
                        </View>
                    )}
                    renderItem={({item: row, index}) => {
                        return(
                            <TouchableOpacity activeOpacity={1} onPress={()=>descontoItemSelected(row)}>
                                <View style={styles.item}>
                                    <Text numberOfLines={1} style={{fontSize:14, color:'black', fontWeight:'bold'}}>{row.store}</Text>
                                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                        <Text style={styles.texto}>Cliente: </Text>
                                        <Text numberOfLines={1} style={{flex:1,  fontSize:13, textTransform:'uppercase', color:'black'}}>{row.client == null ? 'Consumidor Final' : row.client}</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                        <Text style={styles.texto}>Venda:</Text>
                                        <Text style={{color:'black'}}>{row.idSale}</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                        <Text style={styles.texto}>Data:</Text>
                                        <Text style={{color:'black'}}>{row.date}</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                        <Text style={styles.texto}>Total:</Text>
                                        <Text style={{color:'black'}}>R$ {Utils.formatMoeda(row.totalSale)}</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                        <Text style={styles.texto}>Desconto:</Text>
                                        <Text style={{color:'black'}}>R$ {Utils.formatMoeda(row.discountTotal)}</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                        <Text style={styles.texto}>Porcentagem:</Text>
                                        <Text style={{color:'green'}}>{row.discountPercent}%</Text>
                                    </View>

                                    {
                                        row.status != 1 &&
                                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                            <Text style={styles.texto}>Usuário: </Text>
                                            <Text style={{...styles.texto,  fontSize:13, textTransform:'uppercase', color:'black'}}>{row.usuario == null ? '-' : row.usuario}</Text>
                                        </View>
                                    }
                                </View>
                            </TouchableOpacity>
                        )
                    }} />
                }
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    menu: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        borderRadius:10,
        borderColor:Styles.colors.primary,
        borderWidth:1,
        backgroundColor:'white',
        marginBottom:16
    },
    menu_item:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'white',
        padding:7,
        borderRadius:10
    },
    menu_texto_item:{
        fontSize:14, 
        color:'grey'
    },
    input_buscar:{
        padding:10,
        borderRadius:10,
        borderColor:'#c3c3c3',
        borderWidth:1,
        fontSize:16,
        color:'black',
        marginBottom:16
    },
    item:{
        flexDirection:'column',
        justifyContent:'flex-start',
        padding:10,
        backgroundColor:'white',
        borderRadius:5,
        marginBottom:10,
    }
})

export default Lista;