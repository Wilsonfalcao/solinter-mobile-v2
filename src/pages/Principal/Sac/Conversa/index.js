import React, { memo, useContext, useEffect, useRef, useState } from 'react';
import { Alert, Dimensions, SafeAreaView, FlatList, KeyboardAvoidingView, StyleSheet, Text, TextInput, TouchableOpacity, View, Image, Platform } from 'react-native';
import { useHeaderHeight } from '@react-navigation/stack';
import { Styles } from '../../../../config/styles';
import { MaterialIcons } from '@expo/vector-icons';
import * as Linking from 'expo-linking';
import { sendMessageText } from '../../../../services/sac';
import { Utils } from '../../../../config/utils';

import ProfileContext from '../../../../context/ProfileContext';
import Loading from '../../../../components/Loading';
import PopupMenu from '../../../../modals/PopupMenu';

const { width } = Dimensions.get('screen');

const Conversa = ({navigation, route}) => {

    const { dados } = route.params;
    
    const headerHeight = useHeaderHeight();

    const [profile] = useContext(ProfileContext);

    const ref_lista = useRef(null);
    const ref_message = useRef(null);

    const [lista, setLista] = useState([]);
    const [message, setMessage] = useState("");
    const [placeholder, setPlaceholder] = useState("Escreva aqui...");

    const [loading, setLoading] = useState(false);
    const [popup, setPopup] = useState(false);

    useEffect(() => {
        navigation.setOptions({
            headerTitle:() => (
                <View 
                    style={{
                        width:250,
                        height:50,
                        flexDirection:'row',
                        alignItems:"flex-start",
                        justifyContent:'flex-start',
                        marginLeft: -20,
                    }}
                >
                    <Image source={{uri: dados.avatar_cliente}} resizeMode={'cover'} style={{width:40, height:40, borderRadius:20, backgroundColor:'#e6e6e6', marginRight:10}} />
                    <View style={{alignSelf:'flex-start', flex:1,marginTop:Platform.OS === "ios" ? 4 :0}}>
                        <Text numberOfLines={1} style={{fontSize:15, color:'black', fontWeight:'bold'}}>{dados.nome_cliente}</Text>
                        <Text numberOfLines={1} style={{fontSize:11,fontWeight:'400',color:'black'}}>{`Nº Prot.: [${dados.protocolo}]`}</Text>
                    </View>
                </View>
            ),
            headerRight:() => (
                <>
                {
                    dados.status === 0 && 
                    <View style={{alignItems:'center', justifyContent:'flex-start', flexDirection:'row'}}>
                        <TouchableOpacity style={{padding:10}} activeOpacity={0.5} onPress={() => setPopup(true)}>
                            <MaterialIcons name={'more-vert'} color={'black'} size={24} />
                        </TouchableOpacity>
                    </View>
                    
                }
                </>
            )
        })
    },[navigation]);

    const listaMessagens =()=> {

        fetch('https://www.solinter.com.br/v2.0/api/whatsapp/chat/getListTalk', {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${profile.accessToken}`
            },
            body:JSON.stringify({ cd_conversa: dados.id })
        }).then(data=>data.json())
        .then(JSON=>setLista(JSON));
    }

    useEffect(() => {
        listaMessagens();
    },[lista]);
    
    //console.log(lista);

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
            inverted
            data={lista}
            contentContainerStyle={{
                flexGrow: 1, justifyContent: 'flex-end',
            }}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item: row, index}) => {
                return(
                    <View style={{...styles.item, alignSelf:row.de === profile.idUser ? 'flex-end' : 'flex-start', marginBottom: index === (lista.length -1 ) ? 10 : 0}}>
                        <View style={{flexDirection:'column', backgroundColor: row.de === profile.idUser ? 'rgba(70,130,180, 0.2)' : 'rgba(0,179,0, 0.2)' , marginLeft:10, padding:10, borderRadius:10}}>
                            
                            {
                                row.type === "text" &&
                                <Text style={{fontSize:14, color:'black', fontWeight:'500', textAlign:'left',marginBottom:3,}}>{row.message}</Text>
                            }

                            {   
                                row.type === "jpeg" &&
                                <TouchableOpacity activeOpacity={1} onPress={() => openViewFile(row)}>
                                    <Image
                                    source={{uri:row.message}}
                                    style={styles.image}
                                    resizeMode={'cover'} />
                                </TouchableOpacity>
                            }

{   
                                (row.type != "jpeg" && row.type != "text") &&
                                <TouchableOpacity activeOpacity={1} onPress={() => {Linking.openURL(row.message);}}>
                                    <View style={{
                                        justifyContent:"center",
                                        alignItems:"center",
                                        padding:30,
                                        borderColor:"rgb(128,128,128)",
                                        borderWidth:0.8,
                                        borderRadius:10,
                                    }}>
                                        <MaterialIcons name="attach-file" size={30} color="black" />
                                    </View>
                                    <Text style={{
                                        fontSize:12,
                                        color:'rgb(0,128,255)',
                                        fontWeight:'100', 
                                        textAlign:'left',
                                        marginBottom:3,
                                        }}
                                    >
                                        {row.message}
                                    </Text>
                                </TouchableOpacity>
                            }
                            <Text style={{fontSize:10, color:'black', fontWeight:'400', textAlign:row.de === profile.idUser ? 'right' : 'left'}}>{getDate(row.created_at)}</Text>
                        </View>
                    </View>
                )
            }} 
            />
            {
                dados.status === 0 &&
                <KeyboardAvoidingView 
                    behavior={Platform.OS === "ios" ? "padding" : "height"}
                    keyboardVerticalOffset={headerHeight}
                >
                    <View style={styles.box}>
                        <View style={styles.box_input}>
                            <TextInput
                                ref={ref_message}
                                keyboardType={Platform.IO === "ios" ? "twitter" : "default"}
                                value={message}
                                onChangeText={(value) => setMessage(value)}
                                style={styles.text_message}
                                placeholder={placeholder}
                                multiline={true}
                            />
                        </View>
                        <TouchableOpacity style={styles.button_send} activeOpacity={0.5} onPress={() => sendMessage()}>
                            <MaterialIcons name={'send'} color={'white'} size={24} />
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            }
            <PopupMenu visible={popup} close={(dados) => closePopupMenu(dados)} />
            <Loading visible={loading} />
        </SafeAreaView>
    );

    async function sendMessage(){
        if(message == ""){
            ref_message.current.focus()
            setPlaceholder("Escreva antes de enviar...")
        }else{
            setMessage("")
            await registerMessage()
            await sendMessageText(dados.cliente, message)
            listaMessagens()
        }
    }

     //Funções da classe Viewer
    function getDate(time){

        time = new Date(parseInt(time)*1000);
        let today = new Date(Date.now()).getDate();
        let minutes = String(time.getMinutes()).padStart(2, '0');
        
        let timeChat = `${time.getDate()}/${time.getMonth()+1}/${time.getFullYear()}  `+
                        `${time.getHours()}:${minutes}`;
        
        if((time.getDate()-today) === 1){
            timeChat = `ontem às ${time.getHours()}:${minutes}`
        }

        return timeChat;
    }

    async function registerMessage(){
        const req = await fetch('https://www.solinter.com.br/v2.0/api/whatsapp/chat/registerMessage', {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${profile.accessToken}`
            },
            body:JSON.stringify({ 
              id: dados.id,
              message: message  
            })
        });
        return await req.json();
    }

    async function finishAtendant() {

        const req = await fetch('https://www.solinter.com.br/v2.0/api/whatsapp/chat/finalizarAtendimento', {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${profile.accessToken}`
            },
            body:JSON.stringify({ id: dados.id })
        });
        return await req.json();
    }

    async function openViewFile(dados){
        navigation.navigate('ViewFileSac', { dados });
    }

    async function closePopupMenu(info){

        setPopup(false);

        switch (info.action) {
            case "transfer":
                navigation.navigate('Sac_Atendentes', { info, dados });
                break;
                
            case "finish":

                setLoading(true);
                finishAtendant().then(response =>{

                        if(response){
                            Alert.alert("Aviso", "Atendimento finalizado!" ,[{text:'OK'}]);
                            navigation.goBack();
                        }else{

                            Alert.alert("Aviso", "Erro ao finalizar" ,[{text:'OK'}]);
                            navigation.goBack();

                        }
                    }
                );

                setLoading(false);
                break;
            default:
                setPopup(false);
                break;
        }
    }
}

const styles = StyleSheet.create({
    header_title:{
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'flex-start'
    },
    container:{
        flex:1,
        flexDirection:'column'
    },
    lista:{
        flex:1
    },
    box:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        padding:5
    },
    box_input:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        backgroundColor:'white',
        borderRadius:20,
        marginRight:10,
        marginLeft:5,
        elevation:2,
        shadowColor:'grey',
        shadowOffset:{ width:1, height:2},
        shadowOpacity:0.1,
        shadowRadius:0.3
    },  
    text_message:{
        width:"100%",
        marginVertical:10,
        marginHorizontal:8,
        fontSize:16,
        color:'black',
        fontWeight:'500'
    },
    button_camera:{
        marginRight:14,
    },
    button_send:{
        width: (width / 9),
        height: (width / 9),
        borderRadius: (width / 9) / 2,
        backgroundColor:Styles.colors.primary,
        alignItems:'center',
        justifyContent:'center',
        paddingLeft:3,
        marginRight:5,
        elevation:2,
        shadowColor:'grey',
        shadowOffset:{ width:1, height:2},
        shadowOpacity:0.1,
        shadowRadius:0.3
    },
    exit_button:{
        fontSize:16,
        color:'red',
        fontWeight:'700'
    },
    item:{
        
        borderRadius:10,
        flexDirection:'row',
        alignItems:'flex-start',
        justifyContent:'flex-start',
        maxWidth:'80%',
        marginHorizontal:10,
        marginTop:5
    },
    avatar:{
        width: width / 11,
        height: width / 11,
        borderRadius: (width / 11) / 2 
    },
    image:{
        width: (width / 2),
        height: (width / 2),
        borderRadius:10,
        marginBottom:5
    }
})

export default memo(Conversa);