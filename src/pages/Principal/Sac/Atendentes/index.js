import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, View, SafeAreaView, FlatList, TouchableOpacity, Image, Text, Dimensions, ActivityIndicator, StatusBar, Alert} from 'react-native';

import ProfileContext from '../../../../context/ProfileContext';

import Loading from '../../../../components/Loading';
import { API } from '../../../../config/api';

const { width } = Dimensions.get('screen');

const Atendentes = ({route, navigation}) => {

    const { info, dados } = route.params;

    const [profile] = useContext(ProfileContext);
    const [lista, setLista] = useState([]);
    const [modal, setModal] = useState(false);

    useEffect(() => {
        getListaAllAtendants();
    },[]);

    return (
        <SafeAreaView style={{flex:1}}>
            <StatusBar barStyle={'dark-content'} backgroundColor={'white'} />
            <FlatList
            data={lista}
            refreshing={false}
            onRefresh={() => getListaAllAtendants()}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item:row, index}) => (
                <>
                {
                    row.name && 
                    <TouchableOpacity activeOpacity={0.5} onPress={ async () => TransferAtendant(row)}>
                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', padding:10}}>
                            <Image source={{uri: row.photo }} resizeMode={'cover'} style={styles.avatar} />
                            <View style={{flexDirection:"column", marginHorizontal:10}}>
                                <Text numberOfLines={1} style={styles.name}>{row.name}</Text>
                                <Text numberOfLines={1} style={styles.sector}>Setor: {row.sector ? row.sector : 'Não informado'}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                }
                </>
            )} />
            <Loading visible={modal}/>
        </SafeAreaView>
    )

    async function TransferAtendant(row){
        setModal(true);
        const response = await transferAtendant(row.id, dados.id, "Sua conversa foi transferida para "+row.name);
        setModal(false);
        if(response.status === true){
            navigation.pop();
            navigation.pop();
        }else{
            Alert.alert("Aviso", response.msg, [{text:"Ok"}]);
        }        
    }

    async function getListaAllAtendants(){
        const req = await fetch('http://www.solinter.com.br/v2.0/api/whatsapp/chat/getListUser', {
            method:'get',
            headers:{'Content-Type':'application/json', 'token':`${profile.accessToken}`}
        });
        const response = await req.json();
        if(response.status === true){
            setLista(response.users);
        }else{
            setLista([]);
        }
    }

    async function transferAtendant(id_user, id_chat, message_transfer){
        const req = await fetch(API.sac+'/whatsapp/chat/transferirAtendimento', {
            method:'post',
            headers:{
                'Content-Type':'application/json',
                'token':`${profile.accessToken}`
            },
            body:JSON.stringify({ id_user, id_chat, message_transfer })
        });
        return await req.json();
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    avatar:{
        width: (width / 8),
        height: (width / 8),
        borderRadius: (width / 8) / 2,
        backgroundColor: '#e6e6e6'
    },
    name:{
        fontSize:14,
        fontWeight:'bold',
        color:'#303030',
        textTransform:'uppercase'
    },
    sector:{
        fontSize:11,
        color:'#707070',
        textTransform:'uppercase'
    }
})

export default Atendentes;