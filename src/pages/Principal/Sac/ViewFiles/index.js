import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';

const ViewFiles = ({route}) => {

    const  { dados } = route.params;

    const images = [{
        url: dados.message
    }]

    return (
        <View style={styles.container}>
            <StatusBar style={'light'} />
            <ImageViewer 
            renderIndicator={() => (<View></View>)}
            enablePreload={true}
            imageUrls={images}/>
        </View>
    );
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'black',
       flexDirection:'column',
       justifyContent:"center"
   }
})

export default ViewFiles;