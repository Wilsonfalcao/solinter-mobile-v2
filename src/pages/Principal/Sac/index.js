import React, { memo, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { 
    SafeAreaView,
    FlatList,
    StyleSheet,
    View,
    Image, 
    Text, 
    TouchableOpacity, 
    Dimensions, 
    ToolbarAndroidComponent,
    TextInput,
    Platform
} from 'react-native';
import { getLista } from '../../../services/sac';
import { MaterialIcons } from '@expo/vector-icons';
import Header from '../../../components/Header';
import {getNewMessages,InserNewMessages} from "../../../database/orcamentos";
import AsyncStorage from '@react-native-async-storage/async-storage';
import ProfileContext from '../../../context/ProfileContext';
import { Styles } from '../../../config/styles';

const { width } = Dimensions.get('screen');

const Sac = ({navigation}) => {

    const [profile] = useContext(ProfileContext);

    //Variáveis de componentes
    const [lista, setLista] = useState([]);
    const [pesquisa,setPesquisa] = useState("");

    const FlatListAtendimento = useRef();
    const FlatListConcluidos = useRef();
    const buttonConcluidos = useRef();
    const buttonAtendimento = useRef();
    const searchbar = useRef();

    //Registrar Quantidade de Mensagens
    const registerNewsMessages = async (qtd) =>{
        await InserNewMessages(qtd);
    }

    //Contador de Mensagens novas
    const [newMessages,setNewMessageStore] = useState(null);
    getNewMessages(setNewMessageStore);
    const verifyNewsMessages = useMemo(()=>{

        let valor = (lista.length - newMessages);

        return valor;
    });
    const [newMessageApp, setNewMessageApp]= useState(newMessages);

    //Variáveis de componentes (seletores da lista)

    //Variação do estado lista conforme filtro.
    const [refresh, setRefresh] =useState(refresh);
    const [listFilter,setListaFilter] = useState(null);
    const [listConcluidos,setConcluidos] = useState(null);

    useEffect(() => {
        getListaAtendimentos();
    },[lista]);

    //console.log(lista);
    
    return (
        <SafeAreaView style={styles.container}>            
            <Header />
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', marginHorizontal:16, marginTop:10}}>
                <Text style={{flex:1, fontSize:16, color:'#303030', fontWeight:'bold'}}>ATENDIMENTO WHATSAPP</Text>
            </View>
            
            {/* SELETOR DE ESTADO DA LISTA */}
            <View style={[styles.wrapper_menu,{ marginVertical:10}]}>
                
                {/* BOTÕES DE CLASSIFICAÇÃO DE ATENDIMENTO */}
                <TouchableOpacity
                    style={styles.item_menu}
                    onPressIn={()=>{

                        buttonAtendimento.current.setNativeProps({
                            style:{
                                color:'#4682B4',
                                fontWeight:'bold'
                            }
                        });

                        buttonConcluidos.current.setNativeProps({
                            style:{
                                color:'#c3c3c3',
                                fontWeight:'normal'
                            }
                        });

                        FlatListConcluidos.current.setNativeProps({
                            style:{
                                display:"none",
                            }
                        });

                        FlatListAtendimento.current.setNativeProps({
                            style:{
                                display:"flex",
                            }
                        });
                
                    }}
                    onPressOut={ async ()=>{
                        setPesquisa("");
                        

                        setNewMessageApp(lista.length);
                        setNewMessageStore(lista.length);
                        await registerNewsMessages(lista.length);
                    }}
                    onPress={() =>{
                        null;
                    }}>
                        <View style={{
                            flexDirection:"row",
                        }}>
                            <Text 
                                ref={buttonAtendimento}
                                style={{
                                    fontSize:15,
                                    color: "#4682B4",
                                    fontWeight:'bold'}}
                            >
                                Atendimento
                            </Text>
                            {verifyNewsMessages > 0 &&
                                <View style={{
                                    marginLeft:4,
                                    justifyContent:"center",
                                    alignItems:"center",
                                    alignContent:"center",
                                    width:15,
                                    height:15,
                                    backgroundColor:"rgb(255,128,64)",
                                    borderRadius:15,
                                }}>
                                    <Text 
                                    style={{
                                        fontSize:10,
                                        color:"rgb(255,255,255)",
                                        alignItems:"flex-end",
                                        fontWeight:"bold",
                                        }}
                                    >
                                        {verifyNewsMessages}
                                    </Text>
                                </View>
                            }
                        </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPressIn={()=>{

                        buttonConcluidos.current.setNativeProps({
                            style:{
                                color:'#4682B4',
                                fontWeight:'bold'
                            }
                        });

                        buttonAtendimento.current.setNativeProps({
                            style:{
                                color:'#c3c3c3',
                                fontWeight:'normal'
                            }
                        });


                        FlatListConcluidos.current.setNativeProps({
                            style:{
                                display:"flex",
                            }
                        });

                        FlatListAtendimento.current.setNativeProps({
                            style:{
                                display:"none",
                            }
                        });
                
                    }}
                    style={styles.item_menu}
                    onPress={async() =>{
                        setPesquisa("");
                        
                        setNewMessageApp(lista.length);
                        setNewMessageStore(lista.length);
                        await registerNewsMessages(lista.length);
                        }}
                    >
                        <Text 
                            ref={buttonConcluidos}
                            style={{
                                fontSize:15,
                                color: "#c3c3c3",
                                fontWeight:'normal'}}
                        >
                            Concluídos
                        </Text>
                    </TouchableOpacity>
                
            </View>
            
            <View 
                style={{
                    width:"100%",
                    height:"auto",
                }} 
                ref={FlatListAtendimento}
            >
                <FlatList
                data={lista.filter(data=>{
                    return data.status ==0;
                })}
                extraData={refresh}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={()=>{
                    return(
                        <View style={{
                            width:"100%",
                            height:500,
                            justifyContent:"center",
                            alignItems:"center",
                        }}>
                            <Text style={{
                                color:"rgb(0,0,0)",
                                fontWeight:"bold",
                            }}>Não há atendimentos pendentes...</Text>
                        </View>
                    );
                }}
                renderItem={({item}) => {
                    return (
                        <TouchableOpacity activeOpacity={1} onPress={ async () => {
                                
                                //Validando a visualização dos atendimentos abertos e registrando no ambiente
                                setNewMessageApp(lista.length);
                                setNewMessageStore(lista.length);
                                await registerNewsMessages(lista.length);

                                //Abrindo chat
                                navigation.navigate('ConversaSac', {dados:item});
                            }}>
                            <View style={styles.wrapper}>
                                <Image source={{uri: item.avatar_cliente}} style={styles.avatar} resizeMode={'cover'} />
                                <View style={{flex:1, flexDirection:'column', marginLeft:16}}>
                                    <View style={styles.box}>
                                        <Text numberOfLines={1} style={styles.name}>{item.nome_cliente}</Text>
                                        <Text style={styles.date}>{getDate(item.created_at)}</Text>
                                    </View>
                                    <View style={{marginBottom:7,alignItems:"flex-start"}}>
                                        <Text numberOfLines={1} style={styles.textoProtocolo}>{`Nº Prot: ${item.protocolo}`}</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.texto}>{item.ultima_mensagem}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }} />
            </View>

            <View 
            ref={FlatListConcluidos}
                style={{
                    display:"none",
                    flexDirection:"column",
                }}
            >
                <View 
                    style={{
                        width:"92%",
                        height:"auto",
                        marginBottom:10,
                        flexDirection:"row",
                        justifyContent:"center",
                        alignItems:"center",
                    }}>
                    <View style={{
                            zIndex:999,
                            left:Dimensions.get("window").width -40,
                            top:2,
                        }}
                    >
                        <MaterialIcons name={'search'} size={30} color={'rgb(211,211,211)'} />
                    </View>
                    <TextInput
                        value={pesquisa}
                        textAlign={"left"}
                        keyboardType={"web-search"}
                        onChangeText={(value)=>{
                                setPesquisa(value);
                                if(value.length <1){
                                    switchDataByFilters("lista@concluidos");
                                }
                        }}
                        style={{
                            width:"100%",
                            borderRadius:10,
                            borderWidth:1,
                            borderColor:"#c3c3c3",
                            backgroundColor:'white',
                            paddingHorizontal: Platform.OS === "ios" ? 20 : 10,
                            height:50,
                            fontSize:14,
                            color:'rgb(0,0,0)',
                            fontWeight:'500',
                        }}
                        placeholder={"Pesquise protocolo"}
                        onSubmitEditing={() => {switchDataByFilters("lista@busca");}} 
                    />  
                </View>
                <FlatList
                data={
                    !listConcluidos ?

                    lista.filter(data=>{
                        return data.status ==1;
                    })
                    : 
                    listConcluidos
                }
                extraData={refresh}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => {
                    return (
                        <TouchableOpacity activeOpacity={1} onPress={() => {
                                navigation.navigate('ConversaSac', {dados:item});
                            }}>
                            <View style={styles.wrapper}>
                                <Image source={{uri: item.avatar_cliente}} style={styles.avatar} resizeMode={'cover'} />
                                <View style={{flex:1, flexDirection:'column', marginLeft:16}}>
                                    <View style={styles.box}>
                                        <Text numberOfLines={1} style={styles.name}>{item.nome_cliente}</Text>
                                        <Text style={styles.date}>{getDate(item.created_at)}</Text>
                                    </View>
                                    <View style={{marginBottom:7,alignItems:"flex-start"}}>
                                        <Text numberOfLines={1} style={styles.textoProtocolo}>{`Nº Prot: ${item.protocolo}`}</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.texto}>{item.ultima_mensagem}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }} />

            </View>


        </SafeAreaView>
    );

    async function getListaAtendimentos(){

        const response = await getLista(profile.accessToken);

        if(response.length > 0){
            response.sort((a,b)=>a.created_at < b.created_at);
            setLista(response);
        }

        if(lista.length > newMessageApp){
            setNewMessageApp(lista.length);
        }

    }

    //Funções da classe Viewer
    function getDate(time){

        time = new Date(parseInt(time)*1000);
        let today = new Date(Date.now()).getDate();
        let minutes = String(time.getMinutes()).padStart(2, '0');
        
        let timeChat = `${time.getDate()}/${time.getMonth()+1}/${time.getFullYear()}  `+
                        `${time.getHours()}:${minutes}`;
        
        if((time.getDate()-today) === 1){
            timeChat = `ontem às ${time.getHours()}:${minutes}`
        }

        return timeChat;
    }
    
    function switchDataByFilters(type){
        let data_ = [];
        switch (type) {
            case "lista@concluidos":
                data_ = lista.filter(data =>{
                    return data.status === 1;
                })
                data_.sort((a,b)=>a.created_at < b.created_at);
                setConcluidos(data_);
                break;
            
            case "lista@todos":
                data_ = lista;
                data_.sort((a,b)=>a.created_at < b.created_at);
                setListaFilter(data_);
                break;

            case "lista@atendimento":
                data_ = lista.filter(data =>{
                    return data.status === 0;
                })
                data_.sort((a,b)=>a.created_at < b.created_at);
                setListaFilter(data_);
                
                break;

            case "lista@busca":
                data_ = getArrayObjectFilter(lista);
                setConcluidos(data_);
                break;
        
            default:
                data_ = lista.find(data =>{
                    return data.status === 1;
                });
                setListaFilter(data_);
                break;
        }
        setRefresh(!refresh);
    }

    function handleSearch(arr, searchInput){

       return   arr.filter((obj) => (
                Object.values(obj)
                  .flat()
                  .some((v) => (
                      `${v}`.toLowerCase().includes(`${searchInput}`.toLowerCase())
                  ))
                ));
                  
    }

    //Função de filtragem e conversão do Array para retorno de objeto
    function getArrayObjectFilter(data){
        data = JSON.stringify(handleSearch(data,pesquisa));
        data = JSON.parse(data);
        return data;
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,        
        backgroundColor:'#f7f7f7',
    },
    section_title_sucess:{
        alignSelf:'center',
        paddingVertical:2,
        fontSize:15,
        paddingHorizontal:24,
        borderRadius:20,
        borderColor:'green',
        borderWidth:1,
        marginVertical:10,
        color:'green'
    },
    section_title_waiting:{
        alignSelf:'center',
        paddingVertical:2,
        fontSize:15,
        paddingHorizontal:24,
        borderRadius:20,
        borderColor:'rgb(21,67,96)',
        borderWidth:1,
        marginVertical:10,
        color:'rgb(21,67,96)'
    },
    wrapper:{
        backgroundColor:'white',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        padding:20,
        marginHorizontal:16,
        marginBottom:10,
        borderRadius:10,
        elevation:2,
        shadowColor:'grey',
        shadowOffset:{
            width:0,
            height:2
        },
        shadowRadius:0.1,
        shadowOpacity:0.3
    },
    box_title:{
        width:120,
        alignSelf:'center',
        alignContent:'center',
        paddingVertical:2.5,
        borderColor:Styles.colors.primary,
        color:Styles.colors.primary,
        borderWidth:1,
        borderRadius:10,
        textAlign:'center',
        fontSize:14,
        fontWeight:'700',
        marginVertical:16
    },
    item:{
        backgroundColor:'white',
        flexDirection:'column',
        alignItems:'flex-start',
        justifyContent:'flex-start',
        padding:16,
        marginHorizontal:16,
        marginTop:4,
        marginBottom:16,
        borderRadius:10,
        elevation:2,
        shadowColor:'#c3c3c3',
        shadowOffset:{width:1, height:2},
        shadowOpacity:0.2,
        shadowRadius:0.1
    },
    box:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        marginBottom:10
    },
    avatar:{
        width: (width / 7),
        height: (width / 7),
        borderRadius: (width / 7) / 2,
        backgroundColor:'rgba(0,0,0,0.1)'
    },  
    name:{
        flex:1,
        fontSize:15,
        color:"#303030",
        fontWeight:'bold',
        marginRight:10,
    },
    texto:{
        fontSize:14,
        color:'#707070',
        fontWeight:'400',
        marginTop:-3
    },
    textoProtocolo:{
        fontSize:14,
        color:'rgb(170,170,170)',
        fontWeight:'700',
        marginTop:-3,
    },
    date:{
        fontSize:12,
        fontWeight:'500',
        color:'#707070',
    },
    wrapper_menu:{
        marginVertical:10,
        marginHorizontal:16,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        borderRadius:10,
        borderColor:Styles.colors.primary,
        borderWidth:1,
        backgroundColor:'white',
        justifyContent:"space-evenly",
    },
    item_menu:{
        width:120,
        paddingVertical:7.5,
        alignItems:"center",
    },
})

export default memo(Sac);