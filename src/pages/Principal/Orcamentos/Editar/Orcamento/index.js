import React, { useContext, useEffect, useRef, useState, useMemo } from 'react';
import { StyleSheet, View, Text, TextInput, KeyboardAvoidingView, TouchableOpacity, Alert, FlatList, Platform, SafeAreaView } from 'react-native';
import { Menu } from 'react-native-paper';
import { Styles } from '../../../../../config/styles';
import { MaterialIcons } from '@expo/vector-icons';
import { Utils } from '../../../../../config/utils'
import { banco, clearItens, totalOrcamento } from '../../../../../database/orcamentos';
import { estados } from '../../../../../config/strings';

import ProfileContext from '../../../../../context/ProfileContext';
import Orcamentos_Api from '../../../../../services/orcamentos';
import Loading from '../../../../../components/Loading';
import CondicaoPagamentoService from '../../../../../services/condicaopagamento';


const Editar = ({navigation, route}) => {

    const {orcamento} = route.params;

    const produtos = global.bascket[0].data.filter(item=>{return item.type !=2});
    const servicos = global.bascket[1].data.filter(item=>{return item.type ===2});

    const [profile] = useContext(ProfileContext);

    const Api = new Orcamentos_Api(profile.accessToken);
    const Api_CondicaoPagamento = new CondicaoPagamentoService(profile.accessToken);

    const ref_cliente = useRef(null);
    const ref_email = useRef(null);
    const ref_telefone = useRef(null);
    const ref_whatsapp = useRef(null);
    const ref_cidade = useRef(null);
    const ref_valor_entrada = useRef(null);

    const [cliente, setCliente] = useState(orcamento.nameClient);
    const [email, setEmail] = useState(orcamento.emailClient);
    const [telefone, setTelefone] = useState(orcamento.phoneClient);
    const [whatsapp, setWhatsApp] = useState(orcamento.whatsappClient);
    const [cidade, setCidade] = useState(orcamento.cityClient);
    
    const [estado, setEstado] = useState(orcamento.stateClient);
    const [menuEstado, setMenuEstado] = useState(false);
    
    const [condicaoPagamento, setCondicaoPagamento] = useState(null);
    const [listaCondicoesPagamento, setListaCondicoesPagamento] = useState([]);
    const [menuCondicaoPagamento, setMenuCondicaoPagamento] = useState(false);
    

    const [listaParcelas, setListaParcelas] = useState([]);
    const [menuParcelas, setMenuParcelas] = useState(false);
    const [qtdParcelas,setParcelas] = useState(orcamento.parcelsPayment);

    const [valorEntrada,setValorEntrada] = useState(Utils.formatCurrency(orcamento.priceEntryOrcament));

    const [productState, setProduState] = useState(produtos);
    const [servicoState, setServicoState] = useState(servicos);

    const TotalGeral = useMemo(()=>{
        let soma = 0;

        servicoState.map(itens =>{
            soma += (itens.priceItem * itens.qtdItem);
        });

        productState.map(itens =>{
            soma += (itens.priceItem * itens.qtdItem);
        });

        return soma;
    });
    
    const [entrada, setEntrada] = useState(orcamento.entryPayment ? 1 : 0);
    const [menuEntrada, setMenuEntrada] = useState(false);
    const [modal, setModal] = useState(false);

    useEffect(() => {
        return navigation.addListener('focus', function(){
            listaItens();
        });
    },[navigation]);

    useEffect(() => {
        listaItens();
        listaCondicaoPagamento();
    }, []);

    return (
        <SafeAreaView style={{flex:1}}>
            <KeyboardAvoidingView behavior={'padding'} enabled={Platform.OS === "ios"} style={{flex:1}}>
                <FlatList
                style={{flex:1}}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                nestedScrollEnabled={false}
                overScrollMode={'never'}
                keyExtractor={(item, index) => index.toString()}
                removeClippedSubviews
                ListHeaderComponent={(
                    <View style={{marginHorizontal:16, marginTop:16}}>
                        <View style={{flexDirection:'column', marginBottom:10}}>
                            <Text>Cliente</Text>
                            <TextInput
                            ref={ref_cliente}
                            value={cliente}
                            onChangeText={(value) => setCliente(value)}
                            style={styles.input}
                            keyboardType={'default'}
                            returnKeyType={'next'}
                            autoCompleteType={'off'}
                            autoCorrect={false}
                            onSubmitEditing={() => { ref_email.current.focus() }} />
                        </View>
                        <View style={{flexDirection:'column', marginBottom:10}}>
                            <Text>Email</Text>
                            <TextInput
                            ref={ref_email}
                            value={email}
                            onChangeText={(value) => setEmail(value)}
                            style={styles.input}
                            autoCompleteType={'off'}
                            autoCorrect={false}
                            keyboardType={'email-address'}
                            returnKeyType={'next'}
                            onSubmitEditing={() => { ref_telefone.current.focus() }} />
                        </View>
                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                            <View style={{flex:1, flexDirection:'column', marginBottom:10}}>
                                <Text>Telefone</Text>
                                <TextInput
                                ref={ref_telefone}
                                value={telefone}
                                onChangeText={(value) => setTelefone(Utils.mascaraTelefone(value))}
                                style={styles.input}
                                autoCompleteType={'off'}
                                autoCorrect={false}
                                keyboardType={'numeric'} 
                                returnKeyType={'next'}
                                maxLength={15}
                                onSubmitEditing={() => { ref_whatsapp.current.focus() }} />
                            </View>
                            <View style={{marginHorizontal:5}} />
                            <View style={{flex:1, flexDirection:'column', marginBottom:10}}>
                                <Text>WhatsApp</Text>
                                <TextInput
                                ref={ref_whatsapp}
                                value={whatsapp}
                                onChangeText={(value) => setWhatsApp(Utils.mascaraTelefone(value))}
                                style={styles.input}
                                autoCompleteType={'off'}
                                autoCorrect={false}
                                keyboardType={'numeric'} 
                                returnKeyType={'next'}
                                maxLength={15}
                                onSubmitEditing={() => { ref_cidade.current.focus() }} />
                            </View>
                        </View>
                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                            <View style={{flex:1, flexDirection:'column', marginBottom:10}}>
                                <Text>Cidade</Text>
                                <TextInput
                                ref={ref_cidade}
                                value={cidade}
                                onChangeText={(value) => setCidade(value)}
                                style={styles.input}
                                keyboardType={'default'} 
                                returnKeyType={'done'} />
                            </View>
                            <View style={{marginHorizontal:5}} />
                            <View style={{flex:1, flexDirection:'column', marginBottom:10}}>
                                <Text>Estado</Text>
                                <Menu
                                style={{marginTop:Platform.OS === "ios" ? 56 : 24}}
                                visible={menuEstado}
                                onDismiss={() => setMenuEstado(false)}
                                anchor={(
                                    <TouchableOpacity onPress={() => setMenuEstado(true)} activeOpacity={1} style={{...styles.input, flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                        <Text style={{fontSize:14, flex:1}}>{estado ? `${estado}` : 'Selecione'}</Text>
                                        <MaterialIcons name={'arrow-drop-down'} size={20} color={'grey'} />
                                    </TouchableOpacity>
                                )}>
                                {
                                    estados.map((item, index) => <Menu.Item key={index.toString()} title={item.nome} onPress={() => {
                                        setMenuEstado(false);
                                        setEstado(item);
                                    }} />)
                                }
                                </Menu>
                            </View>
                        </View>
                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', marginBottom:10}}>
                            <View style={{flex:0.7, flexDirection:'column'}}>
                                <Text>Condição</Text>
                                <Menu
                                style={{marginTop:Platform.OS === "ios" ? 56 : 24}}
                                visible={menuCondicaoPagamento}
                                onDismiss={() => setMenuCondicaoPagamento(false)}
                                anchor={(
                                    <TouchableOpacity onPress={() => setMenuCondicaoPagamento(true)} activeOpacity={1} style={{...styles.input, flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                        <Text numberOfLines={1} style={{fontSize:14, flex:1}}>{condicaoPagamento ? condicaoPagamento.descricao : 'Selecione'}</Text>
                                        <MaterialIcons name={'arrow-drop-down'} size={20} color={'grey'} />
                                    </TouchableOpacity>
                                )}>
                                {
                                    listaCondicoesPagamento.map((item, index) => (
                                        <Menu.Item style={{flex:1}} key={index.toString()} title={item.descricao} 
                                            onPress={ async () => {
                                                await setMenuCondicaoPagamento(false);
                                                await setCondicaoPagamento(item);
                                                var listaP = [];
                                                for(var i = 1; i <= item.nparcelas; i++){
                                                    listaP.push(`${i}`);
                                                }
                                                setListaParcelas(listaP);
                                        }} />
                                    ))
                                }
                                </Menu>
                            </View>
                            <View style={{marginHorizontal:5}} />
                            <View style={{flex:0.3, flexDirection:'column'}}>
                                <Text>Parcelas</Text>
                                <Menu
                                style={{ minWidth:120, marginTop:Platform.OS === "ios" ? 56 : 24}}
                                visible={menuParcelas}
                                onDismiss={() => setMenuParcelas(false)}
                                anchor={(
                                    <TouchableOpacity onPress={() => setMenuParcelas(true)} disabled={(condicaoPagamento && qtdParcelas !== 0) ? false : true} activeOpacity={1} style={{...styles.input, flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                        <Text style={{fontSize:14, flex:1}}>{orcamento ? `${qtdParcelas}X` : 'Selecione'}</Text>
                                        <MaterialIcons name={'arrow-drop-down'} size={20} color={'grey'} />
                                    </TouchableOpacity>
                                )}>
                                { listaParcelas.map((item, index) => <Menu.Item key={index.toString()} title={`${item}x`} 
                                    onPress={() => {
                                    setMenuParcelas(false);
                                    setParcelas(item);
                                }} />) }
                                </Menu>
                            </View>
                        </View>
                        {
                            ((condicaoPagamento && condicaoPagamento.nparcelas > 1) && qtdParcelas !== 0) &&
                            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                <View style={{flexDirection:'column', marginBottom:10, flex:1}}>
                                    <Text>Entrada</Text>
                                    <Menu
                                    style={{marginTop:Platform.OS === "ios" ? 56 : 24}}
                                    visible={menuEntrada}
                                    onDismiss={() => setMenuEntrada(false)}
                                    anchor={(
                                        <TouchableOpacity onPress={() => setMenuEntrada(true)} activeOpacity={1} style={{...styles.input, minWidth:80, flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                            <Text style={{fontSize:16, flex:1, color:'black'}}>{entrada <= 0 ? 'Selecione' : ( entrada === 0 ? 'Não' : 'Sim')}</Text>
                                            <MaterialIcons name={'arrow-drop-down'} size={20} color={'grey'} />
                                        </TouchableOpacity>
                                    )}>
                                    <Menu.Item title="Sim" onPress={() => { setMenuEntrada(false); setEntrada(1)}} />
                                    <Menu.Item title="Não" onPress={() => { setMenuEntrada(false); setEntrada(0)}} />
                                    </Menu>
                                </View>
                                <View style={{marginHorizontal:5}} />
                                <View style={{flex:1, flexDirection:'column', marginBottom:10}}>
                                    <Text>Valor da Entrada</Text>
                                    <TextInput
                                    editable={entrada === 1 ? true : false}
                                    ref={ref_valor_entrada}
                                    value={valorEntrada}
                                    onChangeText={(value) => setValorEntrada(Utils.formatMoeda(value))}
                                    style={{...styles.input, backgroundColor:entrada === 1 ? 'white' : '#f3f3f3'}}
                                    placeholder={'R$ 0,00'}
                                    keyboardType={'numeric'}
                                    returnKeyType={'done'} />
                                </View>
                            </View>
                        }
                    </View>
                )}
                ListEmptyComponent={(
                    <>
                    {
                        (orcamento) &&
                        <>
                            <View style={{marginHorizontal:16}}>
                                <View style={{...styles.input, flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                    <Text style={{flex:1, fontSize:16}}>Produtos</Text>
                                    <TouchableOpacity activeOpacity={0.5} onPress={() => addProdutos() }>
                                        <Text style={{color:Styles.colors.primary, fontWeight:'bold'}}>Adicionar</Text>
                                    </TouchableOpacity>
                                </View>
                                {
                                    productState.length > 0 ?
                                    <>
                                    {productState.map((item, index) => (
                                            <View key={index.toString()} style={{...styles.item, marginTop: index === 0 ? 10 : 0}}>
                                                <Text style={{fontWeight:'bold'}}>{item.nameItem}</Text>
                                                <Text style={{fontSize:14}}>Quantidade: {item.qtdItem}</Text>
                                                <Text>Valor: R$ {Utils.formatMoeda(item.priceItem.toFixed(2))}</Text>
                                                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                                    <Text style={{flex:1, fontWeight:'bold'}}>Total: R$ {Utils.formatMoeda((item.priceItem * item.qtdItem).toFixed(2))}</Text>
                                                    <TouchableOpacity style={{alignSelf:'flex-end', paddingTop:5}} onPress={() => removeProduct(item)}>
                                                        <Text style={{fontSize:13, color:'red'}}>Remover</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        ))}
                                    </>
                                    :
                                    <>
                                    <Text style={{paddingVertical:16, color:'grey'}}>Não há produtos adicionados</Text>
                                    </>
                                }
                            </View>
                            <View style={{marginBottom:10, marginHorizontal:16}}>
                                <View style={{...styles.input, flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                    <Text style={{flex:1, fontSize:16}}>Serviços</Text>
                                    <TouchableOpacity activeOpacity={0.5} onPress={() => addServivos() }>
                                        <Text style={{color:Styles.colors.primary, fontWeight:'bold'}}>Adicionar</Text>
                                    </TouchableOpacity>
                                </View>
                                {
                                    (servicoState.length > 0) ?
                                    <>
                                    {servicoState.map((item, index) => (
                                            <View key={index.toString()} style={{...styles.item, marginTop: index === 0 ? 10 : 0}}>
                                                <Text style={{fontWeight:'bold'}}>{item.nameItem}</Text>
                                                <Text style={{fontSize:14}}>Quantidade: {item.qtdItem}</Text>
                                                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
                                                    <Text style={{flex:1, fontWeight:'bold'}}>R$ {Utils.formatMoeda(item.priceItem.toFixed(2))}</Text>
                                                    <TouchableOpacity style={{alignSelf:'flex-end', paddingVertical:5}} onPress={() => removeServico(item)}>
                                                        <Text style={{fontSize:13, color:'red'}}>Remover</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        ))}
                                    </>
                                    :
                                    <>
                                    <Text style={{paddingVertical:16, color:'grey'}}>Não há serviços adicionados</Text>
                                    </>
                                }
                            </View>
                        </>
                    }
                    </>
                )}
                ListFooterComponent={(
                    <View style={{marginHorizontal:16, marginBottom:16}}>
                        {
                            (productState.length > 0 || servicoState.length >0) &&
                            <>
                            <View style={{backgroundColor:'white', padding:10, flexDirection:'row', alignItems:'center', marginVertical:16, borderRadius:5}}>
                                <Text style={{flex:1}}>Qtd. Itens: {(productState.length + servicoState.length)}</Text>
                                <Text style={{fontWeight:'bold'}}>Total: R$ {Utils.formatMoeda(TotalGeral.toFixed(2))}</Text>
                            </View>
                            <TouchableOpacity style={styles.button} activeOpacity={0.8} onPress={() => storeEditBudget()}>
                                <Text style={{color:'white'}}>Atualizar</Text>
                            </TouchableOpacity>
                            </>
                        }
                    </View>
                )} />
            </KeyboardAvoidingView>
            <Loading visible={modal} />
        </SafeAreaView>
    )

    //Funções de atualização
    async function listaItens(){
        const dados = await banco();
        if(dados && dados.length > 0){
            dados.filter(item => {

                if(item.type === 1 || item.type === 3){
                    setProduState([...productState,{
                            "custo": null,
                            "idItem": item.id,
                            "imageItem": item.image,
                            "nameItem": item.name,
                            "priceFutureItem": item.price,
                            "priceItem": item.price,
                            "qtdItem": item.qtd,
                            "totalItem":(item.price*item.qtd),
                            "type": 1,
                            "valor_tabela": item.price,
                    }]);
                }

                if(item.type === 2){
                    setServicoState([...servicoState,{
                            "custo": null,
                            "idItem": item.id,
                            "imageItem": item.image,
                            "nameItem": item.name,
                            "priceFutureItem": item.price,
                            "priceItem": item.price,
                            "qtdItem": item.qtd,
                            "totalItem":(item.price*item.qtd),
                            "type": 2,
                            "valor_tabela": item.price,
                    }]);
                }
            });
        }
    }

    //Funções de Componentes
    function removeProduct(row){
        Alert.alert(null, row.nameItem, [
            {text:'Não'},
            {text:'Remover', onPress: async () => {
                global.bascket[0].data = global.bascket[0].data.filter( item => item.idItem !== row.idItem);
                await Promise.all(global.bascket[0].data);
                setProduState(global.bascket[0].data);
            }}
        ])
    }
    function removeServico(row){
        Alert.alert(null, row.nameItem, [
            {text:'Não'},
            {text:'Remover', onPress: async () => {
                global.bascket[1].data = global.bascket[1].data.filter( item => item.idItem !== row.idItem);
                await Promise.all(global.bascket[1].data);
                setServicoState(global.bascket[1].data);
            }}
        ])
    }

    //Funções de Navegação
    function addProdutos(){
        navigation.navigate('Produtos',{ 
            condicaoPagamento: "23",
        });
    }
    function addServivos(){
        navigation.navigate('Servicos', { 
            condicaoPagamento: 23, 
            qtdParcelas
        })
    }

    //Funções da API
    async function storeEditBudget(){

        if(!cliente){
            ref_cliente.current.focus();
            Alert.alert(null, "Informe o nome do cliente",[{text:'Ok'}]);
        }else if(!email){
            ref_email.current.focus();
            Alert.alert(null, "Informe o email do cliente",[{text:'Ok'}]);
        }else if(!whatsapp){
            ref_whatsapp.current.focus();
            Alert.alert(null, "Informe o whatsapp do cliente",[{text:'Ok'}]);
        }else if(!cidade){
            ref_cidade.current.focus();
            Alert.alert(null, "Informe a cidade cliente",[{text:'Ok'}]);
        }else if(!estado){
            Alert.alert(null, "Informe o estado cliente",[{text:'Ok'}]);
        }else if(!condicaoPagamento){
            Alert.alert(null, "Informe a condição de pagamento",[{text:'Ok'}]);
        }else if(qtdParcelas === 0){
            Alert.alert(null, "Informe a quantidade de parcelas",[{text:'Ok'}]);
        }else if(entrada === 1 && valorEntrada == ""){
            Alert.alert(null, "Informe o valor de entrada",[{text:'Ok'}]);
        }else{

            setModal(true);

            orcamento.priceOrcament = TotalGeral;

            var itens = [];
            productState.map( _produto => {
                itens.push({
                    "idItem": _produto.idItem,
                    "qtdItem": _produto.qtdItem,
                    "type" : _produto.type,
                });
            });

            servicoState.map( _servicos => {
                itens.push({
                    "idItem": _servicos.idItem,
                    "qtdItem": _servicos.qtdItem,
                    "type" : _servicos.type,
                    "valor": _servicos.priceItem,
                    "valorFuturo":  _servicos.priceItem,
                });
            });

            var _orcamento = {
                "id" : orcamento.id,
                "nameClient": cliente,
                "emailClient": email,
                "phoneNumber": telefone,
                "zapNumber": whatsapp,
                "cityClient": cidade,
                "stateClient": estado,
                "conditionPayment": condicaoPagamento.id,
                "parcelsPayment": qtdParcelas,
                "entryPayment": entrada != 0 ? true: false,
                "priceEntryPayment": entrada !=0 ? Utils.parseToFloat(valorEntrada) :0,
                "descriptionPayment":condicaoPagamento.descricao,
                "itens": itens
            };

            setModal(false);

            console.log(Utils.parseToFloat(valorEntrada));

            const response = await Api.editOrcamento(_orcamento);
            setModal(false);
            Alert.alert(null, response.msg, [{text:'Ok', onPress:() => { 
                if(response.status === true){
                    navigation.navigate("Lista");
                }   
            }}])
        }
    }

    async function listaCondicaoPagamento(){
        const response = await Api_CondicaoPagamento.getList();
        await setCondicaoPagamento(response.find(data=>{
            return data.id === orcamento.codPayment;
        }))
        setListaCondicoesPagamento(response);
    }
    
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    wrapper:{
        padding:10,
        flexDirection:'column'
    },
    input:{
        borderRadius:5,
        borderWidth:1,
        borderColor:"#c3c3c3",
        backgroundColor:'white',
        height:50,
        paddingHorizontal:10,
        fontSize:14,
        color:'black',
        fontWeight:'500',
        marginTop:5
    },
    item:{
        flexDirection:'column',
        backgroundColor:'white',
        padding:10,
        marginBottom:10,
        borderRadius:5
    },
    button:{
        borderRadius:10,
        backgroundColor:'#00B034',
        alignItems:'center',
        justifyContent:'center',
        padding:14,
        marginTop:16
    }
})

export default Editar;