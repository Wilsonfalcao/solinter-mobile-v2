import React, { useEffect, useState } from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet } from 'react-native';

const Estados = ({navigation, route}) => {

    const close = route.params.close.bind();

    const estados = [
        { uf: 'AC', nome: 'Acre' },
        { uf: 'AL', nome: 'Alagoas' },
        { uf: 'AP', nome: 'Amapá' },
        { uf: 'AM', nome: 'Amazonas' },
        { uf: 'BA', nome: 'Bahia' },
        { uf: 'CE', nome: 'Ceará' },
        { uf: 'DF', nome: 'Distrito Federal' },
        { uf: 'ES', nome: 'Espirito Santo' },
        { uf: 'GO', nome: 'Goiás' },
        { uf: 'MA', nome: 'Maranhão' },
        { uf: 'MS', nome: 'Mato Grosso do Sul' },
        { uf: 'MT', nome: 'Mato Grosso' },
        { uf: 'MG', nome: 'Minas Gerais' },
        { uf: 'PA', nome: 'Pará' },
        { uf: 'PB', nome: 'Paraíba' },
        { uf: 'PR', nome: 'Paraná' },
        { uf: 'PE', nome: 'Pernambuco' },
        { uf: 'PI', nome: 'Piauí' },
        { uf: 'RJ', nome: 'Rio de Janeiro' },
        { uf: 'RN', nome: 'Rio Grande do Norte' },
        { uf: 'RS', nome: 'Rio Grande do Sul' },
        { uf: 'RO', nome: 'Rondônia' },
        { uf: 'RR', nome: 'Roraima' },
        { uf: 'SC', nome: 'Santa Catarina' },
        { uf: 'SP', nome: 'São Paulo' },
        { uf: 'SE', nome: 'Sergipe' },
        { uf: 'TO', nome: 'Tocantins' }
    ]

    return (
        <FlatList
        style={styles.container}
        data={estados}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        overScrollMode={'never'}
        nestedScrollEnabled={false}
        renderItem={({item: row, index}) => { 
            return (
                <TouchableOpacity activeOpacity={0.5} style={styles.item} onPress={() => selectedItem(row)}>
                    <Text style={{fontSize:16, color:'black', fontWeight:'500'}}>{row.uf} - {row.nome}</Text>
                </TouchableOpacity>
            )
        }} />
    )

    function selectedItem(item) {
        close(item);
        navigation.pop();
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:10
    },
    item:{
        backgroundColor:'white',
        marginBottom:10,
        borderRadius:5,
        padding:10,
    }
})

export default Estados;