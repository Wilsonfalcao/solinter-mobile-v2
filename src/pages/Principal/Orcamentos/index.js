import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

import Lista from './Lista';
import Novo from './Novo';
import Produtos from './Novo/Produtos';
import Servicos from './Novo/Servicos';
import Editar from './Editar/Orcamento';
import Detalhes from './Detalhes';
import Item from './Itens/Item';

function Orcamentos() {
  return (
    <Stack.Navigator initialRouteName="Lista">
        <Stack.Screen name="Lista" options={{headerShown:false}} component={Lista} />
        <Stack.Screen name="Novo" options={{title:"Novo Orçamento", headerBackTitleVisible:false}} component={Novo} />
        <Stack.Screen name="Editar" options={{title:"Editar Orçamento", headerBackTitleVisible:false}} component={Editar} />
        <Stack.Screen name="Produtos" options={{title:"Adicionar Produtos", headerBackTitleVisible:false}} component={Produtos} />
        <Stack.Screen name="Servicos" options={{title:"Adicionar Serviços", headerBackTitleVisible:false}} component={Servicos} />
        <Stack.Screen name="Detalhes" options={{title:"Detalhes", headerBackTitleVisible:false}} component={Detalhes} />
        <Stack.Screen name="Item" options={{title:"Inserir Item", headerBackTitleVisible:false}} component={Item} />
    </Stack.Navigator>
  );
}

export default Orcamentos;