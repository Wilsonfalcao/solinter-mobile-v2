import React, { memo, useContext, useEffect, useState } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, SafeAreaView, FlatList } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Styles } from '../../../../config/styles';
import { Utils } from '../../../../config/utils';

import ProfileContext from '../../../../context/ProfileContext';
import Orcamentos_Api from '../../../../services/orcamentos';
import Header from '../../../../components/Header';
import Checkbox from '../../../../components/Checkbox';

const Lista = ({navigation}) => {

    const [profile] = useContext(ProfileContext);

    const Api = new Orcamentos_Api(profile.accessToken);

    const [menuStatus, setMenuStatus] = useState([
        {
            id:0,
            name:'Novos',
            selected:true
        },
        {
            id:1,
            name:'Enviados',
            selected:false
        },
        {
            id:2,
            name:'Lidos',
            selected:false
        }
    ]);

    const [orcamentos, setOrcamentos] = useState([]);

    const [lista, setLista] = useState([]);

    const [status, setStatus] = useState(0);
    const [filtro, setFiltro] = useState(0);

    const [todos, settodos] = useState(true);
    const [negociacao, setnegociacao] = useState(false);
    const [aprovados, setaprovados] = useState(false);
    const [recusados, setrecusados] = useState(false);

    useEffect(() => {
        listaOrcamentos();
    }, []);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            listaOrcamentos();
        });
          return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        changeStatus();
    },[status])

    useEffect(() => {
        changeFiltro();
    },[filtro])

    return (
        <SafeAreaView style={styles.container}> 
            
            <Header />

            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', marginHorizontal:16, marginTop:10, marginBottom:5}}>
                <Text style={{flex:1, fontSize:16, color:'#303030', fontWeight:'bold'}}>ORÇAMENTOS</Text>
                <TouchableOpacity style={{paddingVertical:5}} activeOpacity={0.5} onPress={() => novoOrcamento()}>
                    <MaterialIcons name={'add-circle-outline'} size={25} color={'black'} />
                </TouchableOpacity>
            </View>

            <View style={{...styles.wrapper_menu, marginVertical:0}}>
                { menuStatus.map((item, index) => (
                    <TouchableOpacity 
                    key={index.toString()} 
                    style={styles.item_menu}
                    activeOpacity={1}
                    onPress={() => itemSelectedMenuStatus(item)}>
                        <Text style={{
                            fontSize:15,
                            color: item.selected ? Styles.colors.primary : '#c3c3c3',
                            fontWeight: item.selected ? 'bold' : 'normal'}}>{item.name}</Text>
                    </TouchableOpacity>
                ))}
            </View>

            <View style={{margin:16}}>
                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
                    <Checkbox 
                    select={todos} 
                    setSelect={() => {
                        setFiltro(0);
                        settodos(true);
                        setnegociacao(false);
                        setaprovados(false);
                        setrecusados(false);
                    }} 
                    title={'Todos'} />
                    <Checkbox select={negociacao} setSelect={() => {
                        setFiltro(1);
                        settodos(false);
                        setnegociacao(true);
                        setaprovados(false);
                        setrecusados(false);
                    }} title={'Negociação'} />
                    <Checkbox select={aprovados} setSelect={() => {
                        setFiltro(2);
                        settodos(false);
                        setnegociacao(false);
                        setaprovados(true);
                        setrecusados(false);
                    }} title={'Aprovados'} />
                    <Checkbox select={recusados} setSelect={() => {
                        setFiltro(3);
                        settodos(false);
                        setnegociacao(false);
                        setaprovados(false);
                        setrecusados(true);
                    }} title={'Recusados'} />
                </View>
            </View>

            <FlatList
            data={lista}
            refreshing={false}
            onRefresh={ async () => {
                const orcamentos = await Api.getListAll();
                setOrcamentos(orcamentos); 
                const nova = orcamentos.filter(item => filtro === 0 ? item.status === status : (item.status === status && item.negotiation === filtro));
                await Promise.all(nova);
                setLista(nova)
            }}
            contentContainerStyle={{flexGrow:1}}
            keyExtractor={(item, index) => index.toString()}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            nestedScrollEnabled={false}
            overScrollMode={'never'}
            renderItem={({item: row}) => <ItemOrcamento dados={row} /> }/>
        </SafeAreaView>
    )

    function ItemOrcamento(props){
        const { dados } = props;
        return (
            <TouchableOpacity activeOpacity={1} onPress={() => { navigation.navigate('Detalhes', { dados }) } } >
                <View style={{padding:10, marginHorizontal:10, backgroundColor:'white', marginBottom:10, borderRadius:5}}>
                    <View style={{alignItems:'center', justifyContent:'flex-start', flexDirection:'row'}}>
                        <Text style={{flex:1, fontSize:13, color:'black', fontWeight:'bold'}}>Orçamento: <Text style={{fontWeight:'400'}}>{dados.idOrcamento}</Text></Text>
                        <Text style={{flex:1, fontSize:13, color:'black', fontWeight:'bold'}}>Data: <Text style={{fontWeight:'400'}}>{Utils.timePretty(dados.created_at * 1000)}</Text></Text>
                    </View>
                    <Text style={{flex:1, fontSize:13, color:'black', fontWeight:'bold'}}>Cliente: <Text style={{fontWeight:'400'}}>{dados.nameClient}</Text></Text>
                    <View style={{alignItems:'center', justifyContent:'flex-start', flexDirection:'row'}}>
                        <Text style={{flex:1, fontSize:13, color:'black', fontWeight:'bold'}}>Telefone: <Text style={{fontWeight:'400'}}>{Utils.mascaraTelefone(dados.phoneClient ? dados.phoneClient : dados.whatsappClient)}</Text></Text>
                        <Text style={{flex:1, fontSize:13, color:'black', fontWeight:'bold'}}>Cidade: <Text style={{fontWeight:'400'}}>{dados.cityClient}</Text></Text>
                    </View>
                    <Text style={{flex:1, fontSize:13, color:'black', fontWeight:'bold'}}>Email: <Text style={{fontWeight:'400'}}>{dados.emailClient.toLowerCase()}</Text></Text>
                    <Text style={{flex:1, fontSize:13, color:'black', fontWeight:'bold'}}>Cod. Pagamento: <Text style={{fontWeight:'normal'}}>{`${dados.codPayment} (${dados.conditionPayment})`}</Text></Text>
                    <Text style={{flex:1, fontSize:13, color:'black', fontWeight:'bold'}}>Valor: <Text style={{fontWeight:'400'}}>R$ {Utils.formatMoeda(dados.priceOrcament)}</Text></Text>
                    <Text style={{...styles.btn_status, backgroundColor: statusColorItem(dados.negotiation) }}>{statusItem(dados.negotiation)}</Text>
                </View>
            </TouchableOpacity>
            
        )
    }

    function novoOrcamento(){
        navigation.navigate('Novo');
    }

    async function listaOrcamentos(){
        const orcamentos = await Api.getListAll();
        setOrcamentos(orcamentos); 
        const nova = orcamentos.filter( item => item.status === 0);
        setStatus(0);
        setFiltro(0);
        setLista(nova);
    } 

    function itemSelectedMenuStatus(item) {
        const nova = menuStatus.map((menu) => {
            menu.selected = false
            if(menu.id === item.id){ menu.selected = true }
            return menu
        });
        setStatus(item.id);
        setMenuStatus(nova)
    }

    async function changeStatus(){
        settodos(true);
        setnegociacao(false);
        setaprovados(false);
        setrecusados(false);
        setStatus(status);
        const nova = orcamentos.filter(item => item.status === status);
        await Promise.all(nova);
        setLista(nova);
    }

    async function changeFiltro(){
        const nova = orcamentos.filter(item => filtro === 0 ? item.status === status : (item.status === status && item.negotiation === filtro));
        await Promise.all(nova);
        setLista(nova)
    }

    function statusItem(status){
        switch (status) {
            case 0:
                return "Negociação";
            case 1:
                return "Negociação";
            case 2:
                return "Aprovado";
            case 3:
                return "Recusado";
            default:
                return "";
        }
    }

    function statusColorItem(status){
        switch (status) {
            case 0:
                return Styles.colors.primary;
            case 1:
                return "orange";
            case 2:
                return "green";
            case 3:
                return "red";
            default:
                return "";
        }
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    wrapper_menu:{
        marginVertical:10,
        marginHorizontal:16,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        borderRadius:10,
        borderColor:Styles.colors.primary,
        borderWidth:1,
        backgroundColor:'white'
    },
    box_filtro:{
        padding:20,
        backgroundColor:'white'
    },  
    item_menu:{
        flex:1,
        paddingVertical:7.5,
        alignItems:'center'
    },
    text_item_menu:{
        textAlign:'center',
        fontSize:14,
        color:'grey'
    },
    input:{
        backgroundColor:'white',
        borderRadius:10,
        borderColor:Styles.colors.secondary,
        padding:10,
        fontSize:16,
        color:'black',
        fontWeight:'500',
        marginHorizontal:10,
        marginBottom:10
    },
    btn_remove:{
        marginRight:13,
        borderRadius:5,
        backgroundColor:'red',
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        alignSelf:'flex-end',
        width:100,
        height:120
    },
    btn_status: {
        alignSelf:'flex-start',
        minWidth: 100,
        textAlign:'center',
        fontSize:12,
        color:'white',
        borderRadius:5,
        marginTop:5
    }
})

export default memo(Lista);