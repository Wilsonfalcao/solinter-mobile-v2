import React, { useContext, useEffect, useRef, useState } from 'react';
import { Dimensions, StyleSheet, TextInput, TouchableOpacity, View, Text, KeyboardAvoidingView, Platform } from 'react-native';
import { Menu } from 'react-native-paper';
import Loading from '../../../../../components/Loading';
import { Utils } from '../../../../../config/utils';
import ProfileContext from '../../../../../context/ProfileContext';
import { storeItem } from '../../../../../database/orcamentos';
import Orcamentos_Api from '../../../../../services/orcamentos';

const { width } = Dimensions.get('screen');

const Servicos = ({navigation, route}) => {

    const { condicaoPagamento, parcelas } = route.params;

    const [profile] = useContext(ProfileContext);

    const ref_titulo = useRef();
    const ref_qtd = useRef();
    const ref_valor = useRef();

    const [titulo, setTitulo] = useState("");
    const [qtd, setQtd] = useState("1");
    const [valor, setValor] = useState("");

    const [lista, setLista] = useState([]);
    const [dropDown, setDropDow] = useState(false);
    const [load, setLoad] = useState(false);

    const Api = new Orcamentos_Api(profile.accessToken);

    useEffect(() => {

        if(titulo.length > 2){
            buscarServico().then( dados => {
                console.log(dados);
                if(dados.length > 0){
                    setDropDow(true);
                    setLista(dados);
                }else{
                    setDropDow(false);
                    setLista([]);
                }
            })
        }else{
            setDropDow(false);
            setLista([]);
        }

    }, [titulo]);

    return (
        <KeyboardAvoidingView behavior={'padding'} enabled={Platform.OS === "ios"} style={{margin:16}}>
            <View style={{marginBottom:16}}>
                <Text style={{marginBottom:5, color:'grey'}}>Nome do Serviço</Text>
                <Menu
                style={{minWidth:250, marginTop:Platform.OS === "ios" ? 50 : 20}}
                visible={dropDown}
                onDismiss={() => setDropDow(false)}
                anchor={(
                    <TextInput
                    ref={ref_titulo}
                    value={titulo}
                    onChangeText={(value) => setTitulo(value)}
                    keyboardType={'default'}
                    autoCapitalize={'words'}
                    style={{...styles.input, margin:0}} />
                )}>
                    {
                       (lista && lista.length > 0) && lista.map((item, index) => (<Menu.Item key={index.toString()} title={item.description} onPress={() => {
                        setDropDow(false);
                        ref_qtd.current.focus();
                       }} />))
                    }
                </Menu>

            </View>
            <View style={{marginBottom:16}}>
                <Text style={{marginBottom:5, color:'grey'}}>Quantidade</Text>
                <TextInput
                ref={ref_qtd}
                value={qtd}
                onChangeText={(value) => setQtd(value)}
                keyboardType={Platform.OS === "ios" ? 'numbers-and-punctuation' : 'numeric'}
                returnKeyType={'next'}
                maxLength={6}
                style={{...styles.input, margin:0}}
                onSubmitEditing={() => { ref_valor.current.focus() }} />
            </View>
            <View style={{marginBottom:16}}>
                <Text style={{marginBottom:5, color:'grey'}}>Valor do Serviço</Text>
                <TextInput
                ref={ref_valor}
                value={valor}
                placeholder={'R$ 0,00'}
                onChangeText={(value) => setValor(Utils.formatMoeda(value))}
                keyboardType={'numeric'}
                returnKeyType={'done'}
                style={{...styles.input, margin:0}} />
            </View>
            <TouchableOpacity style={styles.button} activeOpacity={1} onPress={() => adicionarServico()}>
                <Text style={{color:'white'}}>Adicionar</Text>
            </TouchableOpacity>
            <Loading visible={load} />
        </KeyboardAvoidingView>
    )

    async function buscarServico(){
        return await Api.buscarServicos(titulo, condicaoPagamento.id, parcelas);
    }

    async function adicionarServico(){
        setLoad(true);
        const response = await Api.salvarServico(titulo.trim());
        await setLoad(false);
        if(response.length > 0){
            var idItem = response[0].id;
            await storeItem(idItem, parseInt(qtd), 2, titulo.trim(), Utils.convertMoedaToFloat(valor))
            navigation.pop();
        }
    }
}

const styles = StyleSheet.create({
    input:{
        borderRadius:5,
        borderWidth:1,
        borderColor:"#c3c3c3",
        backgroundColor:'white',
        height:50,
        paddingHorizontal:10,
        margin:10,
        fontSize:16,
        color:'black',
        fontWeight:'500'
    },
    item:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        backgroundColor:'white',
        borderRadius:10,
        borderWidth:1,
        borderColor:'#e6e6e6',
        padding:5,
        marginBottom:5,
        marginHorizontal:10
    },
    image:{
        width : (width / 5),
        height: (width / 5),
        borderRadius:5
    },
    title:{
        fontSize:14,
        color:"#303030",
        fontWeight:'600',
        flex:1
    },
    price:{
        fontSize:16,
        color:'black',
        fontWeight:'400'
    },
    button:{
        borderRadius:10,
        backgroundColor:'#00B034',
        alignItems:'center',
        justifyContent:'center',
        padding:14
    }
})

export default Servicos;