import React, { useContext, useState } from 'react';
import { Dimensions, FlatList, StyleSheet, TextInput, TouchableOpacity, View, Image, Text, ActivityIndicator } from 'react-native';
import { Styles } from '../../../../../config/styles';
import { Utils } from '../../../../../config/utils';
import ProfileContext from '../../../../../context/ProfileContext';
import Orcamentos_Api from '../../../../../services/orcamentos';

const { width } = Dimensions.get('screen');

const Produtos = ({navigation, route}) => {

    const { condicaoPagamento, parcelas } = route.params;

    const [profile] = useContext(ProfileContext);

    const [buscar, setBuscar] = useState("");
    const [lista, setLista] = useState([]);
    const [load, setLoad] = useState(false);

    const Api = new Orcamentos_Api(profile.accessToken);

    return (
        <FlatList
        style={{flex:1}}
        data={lista}
        removeClippedSubviews
        contentContainerStyle={{flexGrow:1}}
        stickyHeaderIndices={[0]}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        nestedScrollEnabled={false}
        overScrollMode={'never'}
        ListHeaderComponent={(
            <View style={{backgroundColor:'#f3f3f3'}}>
                <TextInput
                style={styles.input}
                value={buscar}
                onChangeText={(value) => setBuscar(value)}
                placeholder={'Pesquise pelo nome do produto'}
                keyboardType={'default'}
                returnKeyType={'search'}
                onSubmitEditing={() => buscarProduto()} />
            </View>
        )}
        ListEmptyComponent={(
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                { load && <ActivityIndicator color={Styles.colors.primary} size={'large'} /> }
            </View>
        )}
        renderItem={({item: row, index}) => {
            return (
                <TouchableOpacity activeOpacity={0.5} onPress={() => { navigation.navigate('Item', { item: row }) }}>
                    <View style={{...styles.item}}>
                        {
                            row.image ?
                            <Image
                            source={{ uri: row.image }}
                            style={styles.image}
                            resizeMode={'cover'} /> 
                            :
                            <Image
                            source={require('../../../../../../assets/no-image.jpg')}
                            style={styles.image}
                            resizeMode={'cover'} /> 
                        }
                        <View style={{marginLeft:10, flex:1}}>
                            <Text style={styles.title}>{row.name}</Text>
                            <Text style={styles.price}>R$ {Utils.formatMoeda(row.price.toFixed(2))}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        }} />
    )

    async function buscarProduto(){
        setLoad(true);
        setLista([]);
        const response = await Api.buscarProduto(buscar, condicaoPagamento, parcelas);
        setLoad(false);
        if(response.length > 0){
            setLista(response);
        }
    }
}

const styles = StyleSheet.create({
    input:{
        borderRadius:5,
        borderWidth:1,
        borderColor:"#c3c3c3",
        backgroundColor:'white',
        height:50,
        paddingHorizontal:10,
        margin:10,
        fontSize:16,
        color:'black',
        fontWeight:'500'
    },
    item:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        backgroundColor:'white',
        borderRadius:10,
        borderWidth:1,
        borderColor:'#e6e6e6',
        padding:5,
        marginBottom:5,
        marginHorizontal:10
    },
    image:{
        width : (width / 5),
        height: (width / 5),
        borderRadius:5
    },
    title:{
        fontSize:14,
        color:"#303030",
        fontWeight:'600',
        flex:1
    },
    price:{
        fontSize:16,
        color:'black',
        fontWeight:'400'
    }
})

export default Produtos;