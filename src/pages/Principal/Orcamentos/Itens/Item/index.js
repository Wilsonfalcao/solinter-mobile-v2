import React, { useRef, useState } from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity, ScrollView, Alert, TextInput } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Styles } from '../../../../../config/styles';
import { storeItem } from '../../../../../database/orcamentos';
import { Utils } from '../../../../../config/utils';

const Item = ({navigation, route}) => {

    const { item } = route.params;

    const [qtd, setQtd] = useState(1);
    const [price, setPrice] = useState("");

    return (
        <ScrollView style={styles.container}>
            <View style={{backgroundColor:'white', borderRadius:10, padding:16}}>
                
                {
                    item.image &&
                    <Image
                    source={{uri: item.image}}
                    resizeMode={'contain'}
                    style={{aspectRatio:1.8}} />
                }

                <Text style={styles.title}>{item.name}</Text>
                {item.type !== 2 && <Text style={styles.description}>{item.description}</Text>}
                {item.type !== 2 && <Text style={styles.price}>R$ {Utils.formatMoeda(item.price)}</Text>}

                <View style={{marginVertical:16}}>
                    <Text style={{textAlign:'center'}}>Quantidade de itens</Text>
                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', marginTop:24, marginHorizontal:24}}>

                        <TouchableOpacity activeOpacity={0.5} onPress={() => removeItem()}>
                            <MaterialIcons name={'remove-circle'} size={30} color={Styles.colors.primary} />
                        </TouchableOpacity>
                        
                        <TextInput
                        style={{fontSize:22, maxWidth:150, minWidth:100, color:'black', textAlign:'center', backgroundColor:'#f3f3f3', borderRadius:5}}
                        value={qtd.toString()}
                        onChangeText={(value) => {
                            setQtd(value);
                        }}
                        maxLength={4}
                        keyboardType={'numeric'}
                        returnKeyType={'done'} />

                        <TouchableOpacity activeOpacity={0.5} onPress={() => addItem()}>
                            <MaterialIcons name={'add-circle'} size={30} color={Styles.colors.primary} />
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity style={styles.button} activeOpacity={0.5} onPress={() => adicionaItem()}>
                        <Text style={{fontSize:16, color:'white'}}>Adicionar</Text>
                    </TouchableOpacity>

                    {
                        item.type == 2 &&
                        <View style={{margin:16}}>
                            <Text style={{marginBottom:5, fontSize:15}}>Informe o valor</Text>
                            <TextInput
                            ref={ref_price}
                            value={price}
                            onChangeText={(value) => setPrice(Utils.formatMoeda(value))}
                            style={styles.input}
                            placeholder={'R$ 0,00'}
                            keyboardType={'numeric'}
                            returnKeyType={'done'} />
                        </View>
                    }

                </View>
            </View>
        </ScrollView>
    );

    function addItem(){
        var qtd_item = qtd;
        qtd_item = qtd_item + 1;
        setQtd(qtd_item);
    }

    function removeItem(){
        var qtd_item = qtd;
        if(qtd_item <= 1){
            qtd_item = 1;
        }else{
            qtd_item = qtd_item - 1;
        }
        setQtd(qtd_item);
    }

    async function adicionaItem(){
        if(qtd && qtd > 0){
            if(item.type !== 2 || item.price === 0){
                if(item.type !== 2){
                    await storeItem(item.id, qtd, item.type, item.name, item.price,item.image);
                }else{
                    await storeItem(item.id, qtd, item.type, item.name, Utils.convertMoedaToFloat(price));
                }
                navigation.goBack();
            }else{
                Alert.alert("Aviso", "Não é permitido adicionar o item sem valor",[{text:'OK'}])
            }
        }else{
            Alert.alert("Aviso", "Informe a quantidade",[{text:'OK'}])
        }      
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:10
    },
    title:{
        fontSize:14,
        color:'black',
        fontWeight:'600',
        marginBottom:10
    },
    description:{
        fontSize:13,
        color:'grey',
        marginBottom:5,
        fontWeight:'400',
        marginBottom:16
    },
    price:{
        fontSize:16,
        color:'#303030',
        fontWeight:'600'
    },
    button:{
        backgroundColor:Styles.colors.primary,
        marginHorizontal:30,
        padding:12,
        borderRadius:10,
        alignItems:'center',
        justifyContent:'center',
        marginTop:24
    },
    input:{
        backgroundColor:'#f3f3f3',
        padding:10,
        fontSize:22,
        color:'black',
        borderRadius:10
    }
})

export default Item;