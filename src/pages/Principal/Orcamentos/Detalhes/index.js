import React, { useContext, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Alert, SectionList, StyleSheet, Platform } from 'react-native';
import { Menu } from 'react-native-paper';
import { Utils } from '../../../../config/utils';
import { MaterialIcons } from '@expo/vector-icons';
import { clearItens } from '../../../../database/orcamentos';

import ProfileContext from '../../../../context/ProfileContext';
import Orcamentos_Api from '../../../../services/orcamentos';
import Loading from '../../../../components/Loading';
import { Styles } from '../../../../config/styles';

const Detalhes = ({navigation, route}) => {

    {/* RECEBE VALORES DO CABEÇALHO POR PARAMETRO */}
    const { dados } = route.params;

    const [profile] = useContext(ProfileContext);

    const Api = new Orcamentos_Api(profile.accessToken);

    const [lista, setLista] = useState([]);

    const [menu, setMenu] = useState(false);
    const [statusMenu, setStatusMenu] = useState(false);

    const [load, setLoad] = useState(false);
    
    clearItens();

    useEffect(() => {

        navigation.setOptions({
            headerRight:() => (
                <Menu
                    contentStyle={{minWidth:200, marginTop: Platform.OS === "ios" ? 40 : 20}}
                    visible={menu}
                    onDismiss={() => setMenu(false)}
                    anchor={(
                        <TouchableOpacity style={{padding:10}} activeOpacity={1} onPress={() => setMenu(true)}>
                            <MaterialIcons name={'more-vert'} size={24} color={'black'} />
                        </TouchableOpacity>
                    )}>
                    <Menu
                        contentStyle={{width:200, marginTop:20}}
                        visible={statusMenu}
                        onDismiss={() => setStatusMenu(false)}
                        anchor={(
                        <Menu.Item onPress={() => setStatusMenu(true)} title="Alterar Status" />)}>
                        <Menu.Item onPress={() => mudarStatus(2)} title="Aprovar" />
                        <Menu.Item onPress={() => mudarStatus(3)} title="Reprovar" />
                        </Menu>
                        <Menu.Item onPress={() => editar()} title="Editar Orçamento" />
                        <Menu.Item onPress={() => printer()} title="Imprimir" />
                        <Menu.Item onPress={() => enviarEmail()} title="Enviar por Email" />
                        <Menu.Item onPress={() => remover()} title="Excluir" />
                </Menu>
            )
        });
    },[navigation, menu, statusMenu]);

     {/* RECEBER VALORES DA API POR MODIFICAÇÃO */}
    useEffect(() => {
        listaItensOrcamento();
    },[]);

    return (
        <View style={{flex:1}}>
            <SectionList
            sections={lista}
            keyExtractor={(item, index) => index.toString()}
            ListHeaderComponent={(
                <View style={{padding:16}}>
                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:"flex-start"}}>
                        <Text style={{flex:1, fontSize:13, fontWeight:'bold'}}>Orçamento: <Text style={{fontWeight:'normal'}}>{dados.id}</Text></Text>
                        <Text style={{fontWeight:'bold'}}>Data: <Text style={{fontSize:13, fontWeight:'normal'}}>{Utils.timePretty(dados.created_at * 1000)}</Text></Text>
                    </View>
                    <Text style={{fontWeight:'bold'}}>Cliente: <Text style={{fontSize:13, fontWeight:'normal'}}>{dados.nameClient}</Text></Text>
                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:"flex-start"}}>
                        <Text style={{flex:1, fontSize:13, fontWeight:'bold'}}>Telefone: <Text style={{fontWeight:'normal'}}>{Utils.mascaraTelefone(dados.phoneClient)}</Text></Text>
                        <Text style={{fontWeight:'bold'}}>Cidade: <Text style={{fontSize:13, fontWeight:'normal'}}>{dados.cityClient}</Text></Text>
                    </View>
                    <Text style={{fontSize:13, fontWeight:'bold'}}>Email: <Text style={{fontWeight:'normal'}}>{dados.emailClient.toLowerCase()}</Text></Text>
                    <Text style={{fontSize:13, fontWeight:'bold'}}>Cód. Pagamento: <Text style={{fontWeight:'normal'}}>{`${dados.codPayment} (${dados.conditionPayment})`}</Text></Text>
                    <Text style={{marginVertical:5, fontSize:16, fontWeight:"bold"}}>Valor: R$ {Utils.formatMoeda(dados.priceOrcament.toFixed(2))}</Text>                
                    <Text style={{...styles.btn_status, backgroundColor: statusColorItem(dados.negotiation)}}>{statusItem(dados.negotiation)}</Text>
                </View>
            )}
            renderItem={({item: row, index}) => {
                return(
                    <View style={{padding:16}}>
                        <Text style={{fontWeight:'bold', marginBottom:5, fontSize:13}}>{row.idItem} - {row.nameItem}</Text>
                        <Text style={{fontSize:13}}>Quantidade : <Text style={{fontWeight:'bold'}}>{row.qtdItem}</Text></Text>
                        <Text style={{fontSize:13}}>Preço Unitário : <Text style={{fontWeight:'bold'}}>R$ {Utils.formatMoeda(row.priceItem.toFixed(2))}</Text></Text>
                        <Text style={{fontSize:13}}>Total : <Text style={{fontWeight:'bold'}}>R$ {Utils.formatMoeda(row.totalItem.toFixed(2))}</Text></Text>
                    </View>
                )
            }}
            renderSectionHeader={({ section }) => (
                <>
                {section.data.length > 0 &&
                    <View style={{backgroundColor:'#e6e6e6', paddingVertical:10, marginHorizontal:10}}>
                        <Text style={{textAlign:"center", fontSize:14, color:'black'}}>{section.title}</Text>
                    </View>
                }
                </>
            )} />
            
            <Loading visible={load} />  
        </View>
    );
    
    {/* FUNÇÃO DE RECEBIMENTO DE PRODUTOS PELO ID DO ORÇAMENTO. */}
    async function listaItensOrcamento(){  
        const response = await Api.detalisOrcament(dados.idOrcamento);
        var produtos = [];
        var servicos = [];
        var kits = [];
        var lista_orcamento = [];
        response.itens.filter( item => {
            switch (item.type) {
                case 1:
                    produtos.push(item)
                    break;
                case 2:
                    servicos.push(item);
                    break;
                case 3:
                    produtos.push(item)
                    break;
            }
        });
        lista_orcamento.push({title:'Produtos', data: produtos},{title:'Serviços', data: servicos})
        setLista(lista_orcamento);
    }

    function editar(){
        setMenu(false);

        //Filtrando dados salvos passados por API
        const _produto = lista[0].data.filter(item=>{return item.type ===1});
        const _servico = lista[1].data.filter(item=>{return item.type ===2});

        global.bascket = lista;

        //Enviando Valores por Parâmetro
        navigation.navigate('Editar', { 
                orcamento: dados,
            }
        );
    }

    async function enviarEmail(){
        await setMenu(false);
        setLoad(true);
        const response = await Api.sendEmail(dados.idOrcamento);
        await setLoad(false);
        Alert.alert(null, response.msg, [{text:'Ok'}]);
    }

    async function remover(){
        await setMenu(false);
        Alert.alert(null, "Deseja realmente excluir este orçamento?",[
            {text:'Não'},
            {text:'Sim', onPress: async () => {
                    await setLoad(true)
                    const response = await Api.removeOrcament(dados.idOrcamento);
                    await setLoad(false);
                    Alert.alert("Aviso", response.msg, [{text:'Ok', onPress:() => { navigation.pop() }}]);
                }
            }
        ]);
    }

    async function mudarStatus(type){
        await setStatusMenu(false);
        await setMenu(false);
        setLoad(true);
        const response = await Api.changeStatus(dados.idOrcamento, type);
        await setLoad(false);
        Alert.alert(null, response.msg, [{text:'Ok'}]);
    }

    async function printer(){
        await setStatusMenu(false);
        await setMenu(false);
        navigation.navigate('PDF', { id: dados.idOrcamento });
    }

    function statusItem(status){
        switch (status) {
            case 0:
                return "Negociação";
            case 1:
                return "Negociação";
            case 2:
                return "Aprovado";
            case 3:
                return "Recusado";
            default:
                return "";
        }
    }

    function statusColorItem(status){
        switch (status) {
            case 0:
                return Styles.colors.primary;
            case 1:
                return "orange";
            case 2:
                return "green";
            case 3:
                return "red";
            default:
                return "";
        }
    }
    
}

const styles = StyleSheet.create({
    btn_status: {
        alignSelf:'flex-start',
        minWidth: 100,
        textAlign:'center',
        fontSize:12,
        color:'white',
        borderRadius:5,
        marginTop:5
    }
})

export default Detalhes;
