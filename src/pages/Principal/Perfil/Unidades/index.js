import { StatusBar } from 'expo-status-bar';
import React, { useContext, useEffect, useState } from 'react';
import { FlatList, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { Styles } from '../../../../config/styles';
import ProfileContext from '../../../../context/ProfileContext';

const Unidades = ({route, navigation}) => {

    const close = route.params.close.bind();

    const [profile, setProfile] = useContext(ProfileContext);
    const [lista, setLista] = useState([]);


    useEffect(() => {
        if(profile.unidades.length > 0){
            profile.unidades.sort((a, b) => {
                return a.name > b.name
            });
            setLista(profile.unidades);
        }
    },[]);

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={Styles.colors.primary} style={'light'} />
            <FlatList
            data={lista}
            style={styles.lista}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            nestedScrollEnabled={false}
            overScrollMode={'never'}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item: row, index}) => {
                return (
                    <TouchableOpacity style={styles.item} activeOpacity={0.5} onPress={() => unidadeEscolhida(row)}>
                        <Text>{row.name}</Text>
                    </TouchableOpacity>
                )
            }} />
        </View>
    );

    async function unidadeEscolhida(item){
        await close(item.id);
        navigation.pop();
    }

}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column'
    },
    lista:{
        flex:1
    },
    item:{
        backgroundColor:'white',
        padding:16,
        borderColor:'#c3c3c3',
        borderWidth:1,
        borderRadius:10,
        marginHorizontal:16,
        marginTop:16
    }
})

export default Unidades;