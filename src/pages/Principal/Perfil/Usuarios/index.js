import { StatusBar } from 'expo-status-bar';
import React, { useContext, useEffect, useState } from 'react';
import { FlatList, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { Styles } from '../../../../config/styles';
import ProfileContext from '../../../../context/ProfileContext';

const Usuarios = ({route, navigation}) => {

    const { users } = route.params;

    const close = route.params.close.bind();

    const [lista, setLista] = useState([]);

    useEffect(() => {
        if(users.length > 0){
            users.sort((a, b) => {
                return a.name > b.name;
            })
            setLista(users);
        }
    },[]);

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={Styles.colors.primary} style={'light'} />
            <FlatList
            data={lista}
            style={styles.lista}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            nestedScrollEnabled={false}
            overScrollMode={'never'}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item: row, index}) => {
                return (
                    <TouchableOpacity style={styles.item} activeOpacity={0.5} onPress={() => unidadeEscolhida(row)}>
                        <Text>{row.name}</Text>
                    </TouchableOpacity>
                )
            }} />
        </View>
    );

    async function unidadeEscolhida(item){
        await close(item);
        navigation.pop();
    }

}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column'
    },
    lista:{
        flex:1
    },
    item:{
        backgroundColor:'white',
        padding:16,
        borderColor:'#c3c3c3',
        borderWidth:1,
        borderRadius:10,
        marginHorizontal:16,
        marginTop:16
    }
})

export default Usuarios;