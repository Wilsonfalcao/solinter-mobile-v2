import React, { memo, useContext, useEffect, useState } from 'react';
import { Alert, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Styles } from '../../../config/styles';
import { changeInfoPerfil } from '../../../services/perfil';

import Header from '../../../components/Header';
import ProfileContext from '../../../context/ProfileContext';
import MenuDrawerContext from '../../../context/MenuDrawerContext';
import Loading from '../../../components/Loading';


const Perfil = ({navigation}) => {
    
    const [profile, setProfile] = useContext(ProfileContext);
    const [menu, setMenu] = useContext(MenuDrawerContext);

    const [loading, setLoading] = useState(false);
    const [unidade, setUnidade] = useState(profile.unidade);
    const [usuario, setUsuario] = useState({id: profile.idUser, name:profile.name});
    
    return (
        <SafeAreaView style={styles.container}>
            <Header />
            <View style={styles.wrapper}>

                <View style={styles.box}>
                    <Text style={styles.label}>Unidade</Text>
                    <TouchableOpacity style={styles.input} activeOpacity={0.5} onPress={() => abreUnidades()}>
                        <Text style={styles.text_input}>{ getUnidade(unidade) && getUnidade(unidade).name }</Text>
                        <MaterialIcons name={'arrow-right'} size={24} color={'#303030'} />
                    </TouchableOpacity>
                </View>

                <View style={styles.box}>
                    <Text style={styles.label}>Usuário</Text>
                    <TouchableOpacity disabled={ getUnidade(unidade).users.length > 0 ? false : true} style={styles.input} activeOpacity={0.5} onPress={() => abreUsuarios()}>
                        <Text style={styles.text_input}>{usuario.name}</Text>
                        { getUnidade(unidade).users.length > 0 && <MaterialIcons name={'arrow-right'} size={24} color={'#303030'} /> }
                    </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.button} activeOpacity={0.5} onPress={() => updateProfile()}>
                    <Text style={styles.text_button}>Alterar</Text>
                </TouchableOpacity>

            </View>
            <Loading visible={loading} />
        </SafeAreaView>
    );

    async function abreUnidades(){
        navigation.navigate('Unidades', { close: (dados) => setUnidade(dados ? dados : unidade) })
    }

    async function abreUsuarios(){
        navigation.navigate('Usuarios', { 
            users : getUnidade(unidade).users,
            close: (dados) => setUsuario(dados ? dados : usuario) 
        });
    }

    function getUnidade(unidade){
        return profile.unidades.find( item => unidade === item.id);
    }

    async function updateProfile(){
        setLoading(true);
        const dados = await changeInfoPerfil(profile.accessToken, unidade, usuario);
        setLoading(false);
        if(dados.accessToken){ 
            setProfile(dados)
            setMenu(dados.modulos);
            navigation.reset({
                index:0,
                routes:[{name:'Principal'}]
            })
        }
    }

}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'white'
        
    },
    wrapper:{
        flexDirection:'column',
        padding:24
    },
    box:{
        flexDirection:'column',
        marginBottom:16
    },
    label:{
        fontSize:14,
        fontWeight:'400',
        color:'#303030',
        marginBottom:5
    },
    input:{
        backgroundColor:'white',
        padding:16,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        borderRadius:10,
        borderColor:'#c3c3c3',
        borderWidth:1
    },
    text_input:{
        flex:1,
        fontSize:16,
        color:'#303030',
        fontWeight:'500',
        textTransform:'uppercase'
    },
    button:{
        backgroundColor:Styles.colors.primary,
        padding:16,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10,
        marginVertical:16
    },
    text_button:{
        color:'white',
        fontSize:16,
        fontWeight:'400'
    }
})

export default memo(Perfil);