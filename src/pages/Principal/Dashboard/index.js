import React, { memo, useContext } from 'react';
import { StatusBar, StyleSheet, SafeAreaView, View} from 'react-native';
import { WebView } from 'react-native-webview';

import Header from '../../../components/Header';
import ProfileContext from '../../../context/ProfileContext';

function Dashboard() {

  const [profile] = useContext(ProfileContext);

  return (
    <SafeAreaView style={styles.container}>      
      <Header />
      <WebView source={{uri: 'https://www.solinter.com.br/v2.0/dashboard/mobile/'+profile.unidade }} originWhitelist={['*']} />
    </SafeAreaView>
  );

}

const styles = StyleSheet.create({
  container:{
    flex:1,    
    backgroundColor:'white'      
  },
  card_image:{
    aspectRatio:1.3
  }
})

export default memo(Dashboard);