import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import ProfileContext from '../../context/ProfileContext';
import MenuDrawerContext from '../../context/MenuDrawerContext';

import DrawerMenu from './DrawerMenu';
import Descontos from './Descontos';
import Dashboard from './Dashboard';
import Orcamentos from './Orcamentos';
import Sac from './Sac';
import Perfil from './Perfil';

const Drawer = createDrawerNavigator();

function Principal() {

    const [profile, setProfile] = React.useContext(ProfileContext);
    const [menu, setMenu] = React.useState(profile.modulos);

    return (
      <MenuDrawerContext.Provider value={[menu, setMenu]}>
        <Drawer.Navigator drawerContent={(props)=> <DrawerMenu {...props} />} initialRouteName="Dashboard">
          <Drawer.Screen name="Dashboard" component={Dashboard} />
          <Drawer.Screen name="1266" component={Descontos} />
          <Drawer.Screen name="1249" component={Sac} />
          <Drawer.Screen name="1270" component={Orcamentos} />
          <Drawer.Screen name="Perfil" component={Perfil} />
        </Drawer.Navigator>
      </MenuDrawerContext.Provider>
    );
}

export default Principal;