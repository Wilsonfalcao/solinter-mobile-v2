import React, { useState, useRef, useContext } from 'react';
import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View, SafeAreaView, StatusBar, Dimensions, KeyboardAvoidingView } from 'react-native';
import Loading from '../../components/Loading';
import { Styles } from '../../config/styles';
import ProfileContext from '../../context/ProfileContext';
import { validateLogin } from '../../services/login';
import { versao } from '../../config/strings';

const { width } = Dimensions.get('screen');

const Login = ({navigation}) => {

    const [profile, setProfile] = useContext(ProfileContext);

    const ref_email = useRef(null);
    const ref_senha = useRef(null);

    const [email,  setEmail] = useState("");
    const [senha,  setSenha] = useState("");
    const [response, setResponse] = useState(null)

    const[loading,setLoading] = useState({
        visible:false,
        message:null
    });

    async function enviarLogin(){
        setResponse(null)
        if(email == ""){
            setResponse("Informe seu login")
            ref_email.current.focus();
        }else if(senha == ""){
            setResponse("Informe sua senha")
            ref_senha.current.focus();
        }else{
            setLoading({ visible:true, message:"Autorizando usuário" });
            const dados = await validateLogin(email, senha);
            setLoading({ visible:false, message:null }); 
            if(dados.status === false){
                setResponse("Login ou senha inválidos");
            }else{
                setProfile(dados);
                navigation.reset({
                    index:0,
                    routes:[{name:'Principal'}]
                });
            }
        }
    }

    return (
        <View style={styles.container}>
            <SafeAreaView style={{backgroundColor:Styles.colors.primary}} />
            <StatusBar backgroundColor={Styles.colors.primary} barStyle={'light-content'} />
            <View style={{paddingVertical:Dimensions.get('window').width / 24, padding:16}}>
                <Image
                style={styles.logo}
                resizeMode={'contain'}
                source={require('../../../assets/logo.png')} />
            </View>
            <View style={styles.wrapper}>
                <View style={{flex:1, flexDirection:'column'}}>
                    <Text style={{marginBottom:16, fontSize:16, color:'grey'}}>Informe seus dados de acesso</Text>
                    <View style={{marginBottom:16}}>
                        <Text style={{fontSize:16,color:Styles.colors.secondary}}>Login</Text>
                        <TextInput
                        ref={ref_email}
                        style={styles.input}
                        value={email}
                        onChangeText={(value)=>setEmail(value.toLowerCase())}
                        keyboardType={'email-address'}
                        returnKeyType={'next'}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        autoCompleteType={'off'}
                        onSubmitEditing={()=>{ref_senha.current.focus()}} />
                    </View>
                    <View style={{marginBottom:16}}>
                        <Text style={{fontSize:16,color:Styles.colors.secondary}}>Senha</Text>
                        <TextInput
                        ref={ref_senha}
                        style={styles.input}
                        value={senha}
                        onChangeText={(value)=>setSenha(value)}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        autoCompleteType={'off'}
                        keyboardType={'default'}
                        returnKeyType={'done'}
                        secureTextEntry />
                    </View>
                    <TouchableOpacity activeOpacity={0.8} style={styles.button} onPress={()=> enviarLogin()}>
                        <Text style={{fontSize:16, color:'white'}}>Entrar</Text>
                    </TouchableOpacity>
                    <Text style={{color:'red', textAlign:'center', fontSize:16, marginVertical:16}}>{response}</Text>
                </View>
                <View style={{flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontSize:14, color:'black'}}>Solinter</Text>
                    <Text style={{fontSize:11, color:Styles.colors.secondary}}>Todos os direitos reservados</Text>
                    <Text style={{fontSize:11, color:'grey'}}>Versão: {versao}</Text>
                </View>
            </View>
            <Loading visible={loading.visible} message={null} />
            <SafeAreaView style={{backgroundColor:'white'}} />
        </View>
    );
}   

const styles  = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        backgroundColor:Styles.colors.primary,
        
        minHeight:Dimensions.get('window').height
    },
    logo:{
        width:(width / 3),
        height:(width / 3)
    },
    wrapper:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'white',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        paddingHorizontal:24,
        paddingTop:24,
        paddingBottom:16
    },
    input:{
        backgroundColor:'#f3f3f3',
        borderRadius:5,
        borderColor:'grey',
        borderWidth:1,
        marginTop:5,
        padding:16,
        fontSize:20,
        fontWeight:'500',
        color:'black'
    },
    button:{
        backgroundColor:Styles.colors.primary,
        borderRadius:5,
        padding:16,
        alignItems:'center',
        justifyContent:'center',
        marginTop:8,
    }
})

export default Login;