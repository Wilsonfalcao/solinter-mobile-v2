import AsyncStorage from '@react-native-async-storage/async-storage';

async function banco(){
   const dados = await AsyncStorage.getItem("orcamentos");
   return dados ? JSON.parse(dados) : [];
}

async function storeItem(id, qtd, type, name, price, image){
    const dados = await banco();        
    if(dados.length > 0){            
        var lista = dados;
        const index = dados.findIndex(item => item.id === id);
        if(index < 0){
            lista.push({id, type, qtd, name, image, price});
            return await AsyncStorage.setItem("orcamentos", JSON.stringify(lista));
        }else{
            const nova = dados.map( item => {
                if(item.id === id){
                    item.qtd = (item.qtd + qtd)
                }
                return item;
            });
            return await AsyncStorage.setItem("orcamentos", JSON.stringify(nova));
        }
    }else{  
        var lista = [];
        lista.push({id, type, qtd, name, image, price});
        return await AsyncStorage.setItem("orcamentos", JSON.stringify(lista));
    }       
}

async function InserNewMessages(qtd){
    return await AsyncStorage.setItem("newMessages", JSON.stringify(qtd));
}

async function getNewMessages(Dado){
    const dado_ = await AsyncStorage.getItem("newMessages");
    return Dado(JSON.parse(dado_));
}

async function totalOrcamento(){
    const dados = await banco();
    var total = [];
    if(dados && dados.length > 0){
        dados.map(item => {
            if(item.price > 0){
                total.push((item.qtd * item.price))
            }
        });
        var valor = 0;
        total.map( el => {
            valor += el;
        })
        return valor;
    }
    return 0;
}

async function clearItens(){
    await AsyncStorage.removeItem("orcamentos");
    await AsyncStorage.removeItem("bascket");
}

export {
    banco,
    storeItem,
    clearItens,
    totalOrcamento,
    InserNewMessages,
    getNewMessages
}