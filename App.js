import 'react-native-gesture-handler';

import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { LogBox, Platform } from 'react-native';
import { Styles } from './src/config/styles';
import * as Notifications from 'expo-notifications';

import Constants from 'expo-constants';

import Splash from './src/pages/Splash';
import Login from './src/pages/Login';
import Principal from './src/pages/Principal';
import Unidades from './src/pages/Principal/Perfil/Unidades';

import ViewFiles from './src/pages/Principal/Sac/ViewFiles';
import Conversa from './src/pages/Principal/Sac/Conversa';
import Atendentes from './src/pages/Principal/Sac/Atendentes';

import Usuarios from './src/pages/Principal/Perfil/Usuarios';
import ProfileContext from './src/context/ProfileContext';
import Pdf from './src/components/Pdf';
import { Provider } from 'react-native-paper';


const Stack = createStackNavigator();


Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

function App() {  

  const [token, setToken] = useState(null);

  useEffect(() => {
    getPermissoes();
  },[])

  async function getPermissoes() {

    const { status } = await Notifications.getPermissionsAsync();

    if(status === "granted"){
      registerForPushNotificationsAsync().then(tokevalue =>{
        setToken(tokevalue);
      });
    }else{
      const { status } = await Notifications.requestPermissionsAsync();
      if(status !== "granted"){
        getPermissoes()
      }
    }
  }

  async function registerForPushNotificationsAsync() {

    let token;

    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      console.log(token);
    } else {
      alert('Esse dispositivo tem impedimentos para receber notificações');
    }

    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }

    return token;

  }

  function receberNotificacoes(){

    Notifications.addNotificationReceivedListener( notificacao => {
       const { tipo } = notificacao.request.content.data;
       if(tipo == "NovoDesconto"){
         
       }
    })
  }

  const [profile, setProfile] = useState(ProfileContext);

  global.bascket = null;
  global.PushNotificationToken = token;

  LogBox.ignoreAllLogs(true);

  return (
    <Provider>
      <ProfileContext.Provider value={[profile, setProfile]}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName={"Splash"}>

            <Stack.Screen name="Splash" options={{headerShown:false}} component={Splash} />
            <Stack.Screen name="Login" options={{headerShown:false}} component={Login} initialParams={{token}} />
            <Stack.Screen name="Principal" options={{headerShown:false}} component={Principal} />

            {/* Sac */}
            <Stack.Screen name="ConversaSac" options={{headerBackTitleVisible:false, title:null, headerTintColor:Styles.colors.primary}}  component={Conversa} />
            <Stack.Screen name="Sac_Atendentes" options={{title:'Atendentes', headerBackTitleVisible:false}} component={Atendentes} />
            <Stack.Screen name="ViewFileSac" options={{title:null, headerStyle:{backgroundColor:'black', elevation:0, shadowOpacity:0}, headerTintColor:'white', headerBackTitleVisible:false}} component={ViewFiles} />

            {/* Perfil */}
            <Stack.Screen name="Unidades" options={{headerShown:true, title:"Unidades", headerBackTitleVisible:false, headerTintColor:'white', headerStyle:{backgroundColor:Styles.colors.primary}}} component={Unidades} />
            <Stack.Screen name="Usuarios" options={{headerShown:true, title:"Usuários", headerBackTitleVisible:false, headerTintColor:'white', headerStyle:{backgroundColor:Styles.colors.primary}}} component={Usuarios} /> 

            {/* PDF */}
            <Stack.Screen name="PDF" options={{title:'Imprimir', headerBackTitleVisible:false, headerTintColor:'black'}} component={Pdf} /> 
        </Stack.Navigator>
        </NavigationContainer>
      </ProfileContext.Provider>
    </Provider>
  );
}

export default App;


          
          

